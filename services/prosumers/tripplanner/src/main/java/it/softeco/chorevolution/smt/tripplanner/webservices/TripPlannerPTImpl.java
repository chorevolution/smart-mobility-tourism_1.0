package it.softeco.chorevolution.smt.tripplanner.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.utilities.messagepersistence.MessagePersistence;
import eu.chorevolution.utilities.messagepersistence.MessagePersistenceException;
import it.softeco.chorevolution.smt.tripplanner.AllTripsRequest;
import it.softeco.chorevolution.smt.tripplanner.AllTripsResponse;
import it.softeco.chorevolution.smt.tripplanner.ChoreographyInstanceRequest;
import it.softeco.chorevolution.smt.tripplanner.GetBestRouteRequest;
import it.softeco.chorevolution.smt.tripplanner.GetBestRouteResponse;
import it.softeco.chorevolution.smt.tripplanner.GetInfoTransportationRequest;
import it.softeco.chorevolution.smt.tripplanner.GetInfoTransportationResponse;
import it.softeco.chorevolution.smt.tripplanner.ParkingRequest;
import it.softeco.chorevolution.smt.tripplanner.ParkingResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveAllTripsResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveAllTripsResponseReturn;
import it.softeco.chorevolution.smt.tripplanner.ReceiveGetBestRouteRequest;
import it.softeco.chorevolution.smt.tripplanner.ReceiveGetBestRouteRequestReturn;
import it.softeco.chorevolution.smt.tripplanner.ReceiveGetInfoTransportationResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveGetInfoTransportationResponseReturn;
import it.softeco.chorevolution.smt.tripplanner.ReceiveParkingResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveParkingResponseReturn;
import it.softeco.chorevolution.smt.tripplanner.ReceiveTrafficResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveTrafficResponseReturn;
import it.softeco.chorevolution.smt.tripplanner.ReceiveWeatherInformationResponse;
import it.softeco.chorevolution.smt.tripplanner.ReceiveWeatherInformationResponseReturn;
import it.softeco.chorevolution.smt.tripplanner.TrafficRequest;
import it.softeco.chorevolution.smt.tripplanner.TrafficResponse;
import it.softeco.chorevolution.smt.tripplanner.TripPlannerPT;
import it.softeco.chorevolution.smt.tripplanner.WeatherInfoByAreaRequest;
import it.softeco.chorevolution.smt.tripplanner.WeatherInfoByAreaResponse;

@Component(value = "TripPlannerPTImpl")
public class TripPlannerPTImpl implements TripPlannerPT {

	private static Logger logger = LoggerFactory.getLogger(TripPlannerPTImpl.class);
	private MessagePersistence msgPersistence = new MessagePersistence();

	@Override
	public ReceiveGetBestRouteRequestReturn receiveGetBestRouteRequest(ReceiveGetBestRouteRequest parameters) {
		logger.info("CALLED receiveGetBestRouteRequest ON tripplanner");

		try {
			msgPersistence.store("getBestRouteRequest_" + parameters.getChoreographyId().getChoreographyId(), parameters.getGetBestRouteRequest());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveGetBestRouteRequestReturn();
	}

	@Override
	public TrafficRequest sendTrafficRequest(ChoreographyInstanceRequest parameters) {
		logger.info("CALLED sendTrafficRequest ON tripplanner");
		logger.info("parameters.getChoreographyId()=" + parameters.getChoreographyId());

		try {
			GetBestRouteRequest getBestRouteRequest = msgPersistence.get("getBestRouteRequest_" + parameters.getChoreographyId(), GetBestRouteRequest.class);
			//create the bounding box given lat/lon
			BoundingBox bb = BoundingBox.getBoundingBox(getBestRouteRequest.getLat(), getBestRouteRequest.getLon(), getBestRouteRequest.getRange());
			//create trafficRequest
			TrafficRequest trafficRequest = new TrafficRequest();
			trafficRequest.setLlLat(bb.get_llLat());
			trafficRequest.setLlLon(bb.get_llLon());
			trafficRequest.setUrLat(bb.get_urLat());
			trafficRequest.setUrLon(bb.get_urLon());
			return trafficRequest;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public ReceiveTrafficResponseReturn receiveTrafficResponse(ReceiveTrafficResponse parameters) {
		logger.info("CALLED receiveTrafficResponse ON tripplanner");

		try {
			msgPersistence.store("trafficResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getTrafficResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveTrafficResponseReturn();
	}

	@Override
	public AllTripsRequest sendAllTripsRequest(ChoreographyInstanceRequest parameters) {
		logger.info("CALLED sendAllTripsRequest ON tripplanner");
		try {
			GetBestRouteRequest getBestRouteRequest = msgPersistence.get("getBestRouteRequest_" + parameters.getChoreographyId(), GetBestRouteRequest.class);
			AllTripsRequest allTripsRequest = new AllTripsRequest();
			allTripsRequest.setFromLat(getBestRouteRequest.getLat());
			allTripsRequest.setFromLon(getBestRouteRequest.getLon());
			allTripsRequest.setMaxTrips(1);
			allTripsRequest.setModes(getBestRouteRequest.getTripBy());
			allTripsRequest.setStartAt("0607201509:00");
			allTripsRequest.getPoiList().addAll(getBestRouteRequest.getPoiList());
			return allTripsRequest;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public ReceiveAllTripsResponseReturn receiveAllTripsResponse(ReceiveAllTripsResponse parameters) {
		logger.info("CALLED receiveAllTripsResponse ON tripplanner");
		try {
			msgPersistence.store("allTripsResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getAllTripsResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveAllTripsResponseReturn();
	}

	@Override
	public ParkingRequest sendParkingRequest(ChoreographyInstanceRequest parameters) {

		logger.info("CALLED sendParkingRequest ON tripplanner");
		try {
			GetBestRouteRequest getBestRouteRequest = msgPersistence.get("getBestRouteRequest_" + parameters.getChoreographyId(), GetBestRouteRequest.class);
			ParkingRequest parkingRequest = new ParkingRequest();
			parkingRequest.setLat(getBestRouteRequest.getLat());
			parkingRequest.setLon(getBestRouteRequest.getLon());
			parkingRequest.setRange(getBestRouteRequest.getRange());
			return parkingRequest;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public ReceiveParkingResponseReturn receiveParkingResponse(ReceiveParkingResponse parameters) {

		logger.info("CALLED receiveParkingResponse ON tripplanner");
		
		try {
			msgPersistence.store("parkingResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getParkingResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveParkingResponseReturn();
	}

	@Override
	public GetInfoTransportationRequest sendGetInfoTransportationRequest(ChoreographyInstanceRequest parameters) {

		logger.info("CALLED sendGetInfoTransportationRequest ON tripplanner");
		GetInfoTransportationRequest getInfoTransportationRequest = new GetInfoTransportationRequest();
		return getInfoTransportationRequest;
	}

	@Override
	public ReceiveGetInfoTransportationResponseReturn receiveGetInfoTransportationResponse(ReceiveGetInfoTransportationResponse parameters) {

		logger.info("CALLED receiveGetInfoTransportationResponse ON tripplanner");
		try {
			msgPersistence.store("getInfoTransportationResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getGetInfoTransportationResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveGetInfoTransportationResponseReturn();
	}

	@Override
	public WeatherInfoByAreaRequest sendWeatherInformationRequest(ChoreographyInstanceRequest parameters) {

		logger.info("CALLED sendWeatherInformationRequest ON tripplanner");
	try {																  
			GetBestRouteRequest getBestRouteRequest = msgPersistence.get("getBestRouteRequest_" + parameters.getChoreographyId(), GetBestRouteRequest.class);
			WeatherInfoByAreaRequest weatherInfoByAreaRequest = new WeatherInfoByAreaRequest();
			weatherInfoByAreaRequest.setPeriod(36000);
			weatherInfoByAreaRequest.setLat(getBestRouteRequest.getLat());
			weatherInfoByAreaRequest.setLon(getBestRouteRequest.getLon());
			weatherInfoByAreaRequest.setRadius(6000);
			weatherInfoByAreaRequest.setTag("ACQ");
			return weatherInfoByAreaRequest;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public ReceiveWeatherInformationResponseReturn receiveWeatherInformationResponse(ReceiveWeatherInformationResponse parameters) {
		logger.info("CALLED receiveWeatherInformationResponse ON tripplanner");
		
		try {
			msgPersistence.store("weatherInformationResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getWeatherInformationResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getLocalizedMessage());
		}
		return new ReceiveWeatherInformationResponseReturn();
	}

	@Override
	public GetBestRouteResponse sendGetBestRouteResponse(ChoreographyInstanceRequest parameters) {
		
		try {
			GetBestRouteResponse result = new GetBestRouteResponse();
			//pick all the responses saved
			GetBestRouteRequest getBestRouteRequest = msgPersistence.get("getBestRouteRequest_" + parameters.getChoreographyId(), GetBestRouteRequest.class);
			TrafficResponse trafficResponse = null;
			AllTripsResponse allTripsResponse = null;
			ParkingResponse parkingResponse = null;
			GetInfoTransportationResponse infoResponse = null;
			WeatherInfoByAreaResponse weatherResponse = null;
			// trip by walk
			// set getBestRouteResponse value based on trafficResponse, weatherResponse						
			if(getBestRouteRequest.getTripBy()==1){
				allTripsResponse = msgPersistence.get("allTripsResponse_" + parameters.getChoreographyId(), AllTripsResponse.class);
				weatherResponse = msgPersistence.get("weatherInformationResponse_" + parameters.getChoreographyId(), WeatherInfoByAreaResponse.class);
				
				if(!weatherResponse.getDocs().isEmpty())
				{
					result.getWeather().add(weatherResponse.getDocs().get(0));
				}
				if(!allTripsResponse.getTrips().isEmpty())
				{
					result.getTrips().addAll(allTripsResponse.getTrips());
				}
			}
			// trip by bus
			// set getBestRouteResponse value based on trafficResponse, infoResponse
			if(getBestRouteRequest.getTripBy()==4){
				allTripsResponse = msgPersistence.get("allTripsResponse_" + parameters.getChoreographyId(), AllTripsResponse.class);
				infoResponse = msgPersistence.get("getInfoTransportationResponse_" + parameters.getChoreographyId(), GetInfoTransportationResponse.class);
				
				if(!infoResponse.getInfoItem().isEmpty())
				{
					result.getPublicTransportationInfos().addAll(infoResponse.getInfoItem());
				}
				if(!allTripsResponse.getTrips().isEmpty())
				{
					result.getTrips().addAll(allTripsResponse.getTrips());
				}
			}	
			// trip by car
			// set getBestRouteResponse value based on trafficResponse, allTripsResponse, parkingResponse
			if(getBestRouteRequest.getTripBy()==8){
				trafficResponse = msgPersistence.get("trafficResponse_" + parameters.getChoreographyId(), TrafficResponse.class);				
				allTripsResponse = msgPersistence.get("allTripsResponse_" + parameters.getChoreographyId(), AllTripsResponse.class);
				parkingResponse = msgPersistence.get("parkingResponse_" + parameters.getChoreographyId(), ParkingResponse.class);
				
				if(!parkingResponse.getParkings().isEmpty())
				{
					result.getParkings().addAll(parkingResponse.getParkings());
				}
				if(!trafficResponse.getTrafficMessage().isEmpty())
				{
					result.getTrafficMessage().addAll(trafficResponse.getTrafficMessage());
				}
				if(!allTripsResponse.getTrips().isEmpty())
				{
					result.getTrips().addAll(allTripsResponse.getTrips());
				}
			}			
																		   
			return result;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getLocalizedMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
}

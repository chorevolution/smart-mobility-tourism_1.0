package it.softeco.chorevolution.smt.tripplanner.webservices;

public class BoundingBox {

	private static final double km2degree = 0.0089982311916;

	private double urLat;
	private double urLon;
	private double llLat;
	private double llLon;

	public BoundingBox(double urLat, double urLon, double llLat, double llLon) {
		this.urLat = urLat;
		this.urLon = urLon;
		this.llLat = llLat;
		this.llLon = llLon;
	}

	public static BoundingBox getBoundingBox(double lat, double lon, int vicinityRange) {
		
		// convert m to km
		double vicinityRangeToKM = (double) vicinityRange / 1000;
		//calculate boundingBox
		double left = lon - (vicinityRangeToKM * km2degree);
		double right = lon + (vicinityRangeToKM * km2degree);
		double bottom = lat - (vicinityRangeToKM * km2degree);
		double top = lat + (vicinityRangeToKM * km2degree);

		BoundingBox bb = new BoundingBox(top, right, bottom, left);
		return bb;
	}

	public double get_llLon() {
		return this.llLon;
	}

	public double get_llLat() {
		return this.llLat;
	}

	public double get_urLat() {
		return this.urLat;
	}

	public double get_urLon() {
		return this.urLon;
	}
}

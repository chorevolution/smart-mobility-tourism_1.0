package it.softeco.chorevolution.smt.touristagent.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.utilities.messagepersistence.MessagePersistence;
import eu.chorevolution.utilities.messagepersistence.MessagePersistenceException;
import it.softeco.chorevolution.smt.touristagent.ChoreographyInstanceRequest;
import it.softeco.chorevolution.smt.touristagent.GetBestRouteRequest;
import it.softeco.chorevolution.smt.touristagent.GetBestRouteResponse;
import it.softeco.chorevolution.smt.touristagent.GetInfoNewsRequest;
import it.softeco.chorevolution.smt.touristagent.GetInfoNewsResponse;
import it.softeco.chorevolution.smt.touristagent.GetPoiListRequest;
import it.softeco.chorevolution.smt.touristagent.GetPoiListResponse;
import it.softeco.chorevolution.smt.touristagent.GetTripPlanRequest;
import it.softeco.chorevolution.smt.touristagent.GetTripPlanResponse;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetBestRouteResponse;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetBestRouteResponseReturn;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetInfoNewsResponse;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetInfoNewsResponseReturn;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetPoiListResponse;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetPoiListResponseReturn;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetTripPlanRequest;
import it.softeco.chorevolution.smt.touristagent.ReceiveGetTripPlanRequestReturn;
import it.softeco.chorevolution.smt.touristagent.ReceiveStartTrackingRequest;
import it.softeco.chorevolution.smt.touristagent.ReceiveStartTrackingRequestReturn;
import it.softeco.chorevolution.smt.touristagent.StartTrackingResponse;
import it.softeco.chorevolution.smt.touristagent.TouristAgentPT;


@Component(value = "TouristAgentPTImpl")
public class TouristAgentPTImpl implements TouristAgentPT {

	private static Logger logger = LoggerFactory.getLogger(TouristAgentPTImpl.class);
	private MessagePersistence msgPersistence = new MessagePersistence();
	
	@Override
	public GetPoiListRequest sendGetPoiListRequest(ChoreographyInstanceRequest parameters) {

		logger.info("CALLED sendGetPoiListRequest ON touristagent");
		
		try {
			GetTripPlanRequest getTripPlanRequest = msgPersistence.get("getTripPlanRequest_" + parameters.getChoreographyId(), GetTripPlanRequest.class);
			GetPoiListRequest getPoiListRequest = new GetPoiListRequest();
			getPoiListRequest.setLat(getTripPlanRequest.getLat());
			getPoiListRequest.setLon(getTripPlanRequest.getLon());
			getPoiListRequest.setRange(getTripPlanRequest.getRange());
			getPoiListRequest.getTag().addAll(getTripPlanRequest.getTag());
			return getPoiListRequest;		
		} 
		catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	public ReceiveGetPoiListResponseReturn receiveGetPoiListResponse(ReceiveGetPoiListResponse parameters) {

		logger.info("CALLED receiveGetPoiListResponse ON touristagent");
		try {
			msgPersistence.store("getPoiListResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getGetPoiListResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}	
		return new ReceiveGetPoiListResponseReturn();
	}
	
	@Override
	public GetTripPlanResponse sendGetTripPlanResponse(ChoreographyInstanceRequest parameters) {
		
		logger.info("CALLED sendGetTripPlanResponse ON touristagent");
		try {
			GetBestRouteResponse getBestRouteResponse = msgPersistence.get("getBestRouteResponse_" + parameters.getChoreographyId(), GetBestRouteResponse.class);
			GetInfoNewsResponse getInfoNewsResponse = msgPersistence.get("getInfoNewsResponse_"+ parameters.getChoreographyId(), GetInfoNewsResponse.class);
			GetPoiListResponse getPoiListResponse = msgPersistence.get("getPoiListResponse_" + parameters.getChoreographyId(), GetPoiListResponse.class); 
			GetTripPlanResponse getTripPlanResponse = new GetTripPlanResponse();
			getTripPlanResponse.getNews().addAll(getInfoNewsResponse.getInfoItem());
			getTripPlanResponse.getParkings().addAll(getBestRouteResponse.getParkings());
			getTripPlanResponse.getPoi().addAll(getPoiListResponse.getPoi());
			getTripPlanResponse.getPublicTransportationInfos().addAll(getBestRouteResponse.getPublicTransportationInfos());
			getTripPlanResponse.getTrafficMessage().addAll(getBestRouteResponse.getTrafficMessage());
			getTripPlanResponse.getTrips().addAll(getBestRouteResponse.getTrips());
			getTripPlanResponse.getWeather().addAll(getBestRouteResponse.getWeather());
			return getTripPlanResponse;
			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	public ReceiveGetBestRouteResponseReturn receiveGetBestRouteResponse(ReceiveGetBestRouteResponse parameters) {
		
		logger.info("CALLED receiveGetBestRouteResponse ON touristagent");
		try {
			msgPersistence.store("getBestRouteResponse_" + parameters.getChoreographyId().getChoreographyId(), parameters.getGetBestRouteResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveGetBestRouteResponseReturn();
	}
	
	@Override
	public GetInfoNewsRequest sendGetInfoNewsRequest(ChoreographyInstanceRequest parameters) {
		
		logger.info("CALLED sendGetInfoNewsRequest ON touristagent");
		GetInfoNewsRequest getInfoNewsRequest = new GetInfoNewsRequest();
		return getInfoNewsRequest;
	}
	
	@Override
	public StartTrackingResponse sendStartTrackingResponse(ChoreographyInstanceRequest parameters) {
		
		logger.info("CALLED sendStartTrackingResponse ON touristagent");
		try {
			GetBestRouteResponse getBestRouteResponse = msgPersistence.get("getBestRouteResponse_" + parameters.getChoreographyId(), GetBestRouteResponse.class);
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		//TODO
		// create startTrackingResponse from getBestRouteResponse
		// is the right logic???		
		StartTrackingResponse startTrackingResponse = new StartTrackingResponse();
		return startTrackingResponse;
	}
	
	@Override
	public ReceiveStartTrackingRequestReturn receiveStartTrackingRequest(ReceiveStartTrackingRequest parameters) {
	
		logger.info("CALLED receiveStartTrackingRequest ON touristagent");
		try {
			msgPersistence.store("startTrackingRequest_" + parameters.getChoreographyId().getChoreographyId(), parameters.getStartTrackingRequest());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveStartTrackingRequestReturn();
	}
	
	@Override
	public GetBestRouteRequest sendGetBestRouteRequest(ChoreographyInstanceRequest parameters) {
		
		logger.info("CALLED sendGetBestRouteRequest ON touristagent");
		try {
			GetTripPlanRequest getTripPlanRequest = msgPersistence.get("getTripPlanRequest_" + parameters.getChoreographyId(), GetTripPlanRequest.class);
			GetPoiListResponse getPoiListResponse = msgPersistence.get("getPoiListResponse_" + parameters.getChoreographyId(), GetPoiListResponse.class);
			GetBestRouteRequest getBestRouteRequest = new GetBestRouteRequest();
			getBestRouteRequest.setLat(getTripPlanRequest.getLat());
			getBestRouteRequest.setLon(getTripPlanRequest.getLon());
			getBestRouteRequest.setTripBy(getTripPlanRequest.getTripBy());
			getBestRouteRequest.setRange(getTripPlanRequest.getRange());
			getBestRouteRequest.getPoiList().addAll(getPoiListResponse.getPoi());
			return getBestRouteRequest;			
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	public ReceiveGetTripPlanRequestReturn receiveGetTripPlanRequest(ReceiveGetTripPlanRequest parameters) {
		
		logger.info("CALLED receiveGetTripPlanRequest ON touristagent");
		try {
			msgPersistence.store("getTripPlanRequest_" + parameters.getChoreographyId().getChoreographyId(), parameters.getGetTripPlanRequest());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveGetTripPlanRequestReturn();
	}
	
	@Override
	public ReceiveGetInfoNewsResponseReturn receiveGetInfoNewsResponse(ReceiveGetInfoNewsResponse parameters) {
		
		logger.info("CALLED receiveGetInfoNewsResponse ON touristagent");
		try {
			msgPersistence.store("getInfoNewsResponse_"+ parameters.getChoreographyId().getChoreographyId(), parameters.getGetInfoNewsResponse());
		} catch (MessagePersistenceException e) {
			logger.error(e.getMessage(),e);
		}
		return new ReceiveGetInfoNewsResponseReturn();
	}


}

package it.softeco.chorevolution.smt.journeyplanner.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.journeyplanner.AllTripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.AllTripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.JourneyPlannerPT;
import it.softeco.chorevolution.smt.journeyplanner.business.BusinessException;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Component(value = "JourneyPlannerPTImpl")
public class JourneyPlannerPTImpl implements JourneyPlannerPT {

	private static Logger logger = LoggerFactory.getLogger(JourneyPlannerPTImpl.class);
	
	@Autowired
	private JourneyPlannerService service;
		
	@Override
	public AllTripsResponse allTrips(AllTripsRequest parameters) {
		
		logger.info("CALLED allTrips ON journeyplanner");
		
		try {
			//CallRestService c = new CallRestService();
			
			AllTripsResponse resp = service.allTrips(parameters);
        	return resp;
        } 
    	catch (BusinessException ex) {
            logger.error("error", ex);
    		throw new RuntimeException(ex.getMessage());
        }

	}
}

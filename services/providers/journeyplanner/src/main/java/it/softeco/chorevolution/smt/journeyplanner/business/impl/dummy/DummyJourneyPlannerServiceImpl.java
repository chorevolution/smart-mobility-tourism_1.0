package it.softeco.chorevolution.smt.journeyplanner.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.journeyplanner.AllTripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.AllTripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.business.BusinessException;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Service
public class DummyJourneyPlannerServiceImpl implements JourneyPlannerService {
	
	@Override
	public AllTripsResponse allTrips(AllTripsRequest parameters) throws BusinessException {
		AllTripsResponse response = new AllTripsResponse();
		return response;
	}
	
}

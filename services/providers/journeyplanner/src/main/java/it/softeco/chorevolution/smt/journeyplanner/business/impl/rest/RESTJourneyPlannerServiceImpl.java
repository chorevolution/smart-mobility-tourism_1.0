package it.softeco.chorevolution.smt.journeyplanner.business.impl.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.journeyplanner.AllTripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.AllTripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.BusDetailsType;
import it.softeco.chorevolution.smt.journeyplanner.PoiType;
import it.softeco.chorevolution.smt.journeyplanner.PositionType;
import it.softeco.chorevolution.smt.journeyplanner.SegmentType;
import it.softeco.chorevolution.smt.journeyplanner.TripInformationsType;
import it.softeco.chorevolution.smt.journeyplanner.business.BusinessException;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Service
public class RESTJourneyPlannerServiceImpl implements JourneyPlannerService {

	private static Logger logger = LoggerFactory.getLogger(RESTJourneyPlannerServiceImpl.class);

	//base url to contact the journeyPlanner service
	@Value("#{cfgproperties.journeyplannerurl}")
	private String journeyplannerurl;

	public String buildUrl(List<PoiType> poiList, String session, Integer modes, int from, int to)
	{
		 return journeyplannerurl +
				"?fromLat=" + poiList.get(from).getLat() +
				"&fromLon=" + poiList.get(from).getLon() +
				"&toLat=" + poiList.get(to).getLat() +
				"&toLon=" + poiList.get(to).getLon() +
				"&s=" + session +
				"&maxTrips=" + 1 +
				"&modes=" + modes +
				"&startAt=" + "0607201509:00";
	}
	
	public void setTripInfo(JSONArray trips, TripInformationsType[][] tripDetails, int i, int j)
	{
		if(trips.length() > 0)
		{
			//duration
			tripDetails[i][j].setDuration(trips.getJSONObject(0).getInt("du"));
			//segments of the trip
			List<SegmentType> segments = new ArrayList<SegmentType>();
			//pick segments from the json
			JSONObject trip = trips.getJSONObject(0);
			JSONArray segs = trip.getJSONArray("segs");
			//save segments
			for (int y=0; y<segs.length(); y++)
			{
				JSONObject current = segs.getJSONObject(y);
				SegmentType s = new SegmentType();
				//set type of the segment
				switch (current.getInt("t"))
				{
					case 1:
						s.setType("Walk");
						break;
					case 4:
						s.setType("Car");
						break;
					case 32:
						s.setType("Bus");
						break;
					default:
						break;
				}
				
				//set pathPolygon of the segment
				List<PositionType> route = new ArrayList<PositionType>();
				String path = current.getJSONObject("po").getString("c");
				String[] lonLats = path.split("\\|");
				//for each couple, build a position object
				for (String coords : lonLats) 
				{
					PositionType p = new PositionType();
					p.setLat(Double.parseDouble(coords.split(",")[1]));
					p.setLon(Double.parseDouble(coords.split(",")[0]));
					route.add(p);
				}
				//add positions
				s.getPositions().addAll(route);
				//build the busDetails(if present)
				BusDetailsType busDetails = new BusDetailsType();
				if(!current.isNull("v"))
				{
					JSONObject bus = current.getJSONObject("v");
						
					busDetails.setDirection(bus.getString("d"));
					busDetails.setDropInNode(bus.getString("s"));
					busDetails.setDropOffNode(bus.getString("e"));
					busDetails.setLine(bus.getString("l"));
					s.setBusDetails(busDetails);
					
				}
				segments.add(s);
			}
			
			//add details
			tripDetails[i][j].getSegments().addAll(segments);
		}
	}
	
	@Override
	public AllTripsResponse allTrips(AllTripsRequest plannerRequest) 
	{
		// put the starting poi at the beginning of the poiList
		PoiType startingPoi = new PoiType();
		startingPoi.setLat(plannerRequest.getFromLat());
		startingPoi.setLon(plannerRequest.getFromLon());
		startingPoi.setName("Current Position");
		plannerRequest.getPoiList().add(0, startingPoi);

		//the object containing the durations and the details of the trips
		TripInformationsType[][] tripDetails = new TripInformationsType[plannerRequest.getPoiList().size()][plannerRequest.getPoiList().size()];
		
		//initialize matrix of informations
		for(int i=0; i<plannerRequest.getPoiList().size(); i++)
		{
			for(int j=0; j<plannerRequest.getPoiList().size(); j++)
			{
				tripDetails[i][j]= new TripInformationsType();
			}
		}
		//fill the matrix
		for (int i = 0; i < plannerRequest.getPoiList().size(); i++) 
		{
			for (int j = 0; j < plannerRequest.getPoiList().size(); j++)
			{
				try {
					fillMatrix(i, j, plannerRequest.getPoiList(), plannerRequest.getModes(), plannerRequest.getSession(), tripDetails);
				} 
				catch (IOException e) {
					throw new BusinessException(e.getMessage());
				}
			}
		}
		//find the best route
    	try {
			List<TripInformationsType> trips = findRoute(plannerRequest.getModes(), plannerRequest.getSession(), tripDetails, plannerRequest.getPoiList());
			AllTripsResponse resp = new AllTripsResponse();
			resp.getTrips().addAll(trips);
			return resp;
		}
    	catch (IOException e) {
    		throw new BusinessException(e.getMessage());
		}
	}
	
	public void fillMatrix(int i, int j, List<PoiType> poiList, Integer modes, String session, TripInformationsType[][] tripDetails) throws IOException
	{
			//if poi with itself, duration is 0
			if (i == j) 
			{
				tripDetails[i][j].setDuration(0); 
			}
			else 
			{
				//build the url and call the journeyPlanner.trips
				String url = buildUrl(poiList, session, modes, i, j);
				//get json response from url
				String json;
				json = getJson(url);
				//parse the json response and save the duration of the trip in the matrix
				JSONObject obj = new JSONObject(json);
				
				//if no trips found with car transport type, try to search for bus
				if(obj.get("ts").toString().equals("null") && modes==8)
				{
					fillMatrix(i, j, poiList, 4, session, tripDetails);
					return;
				}
				//if trips found, save the duration and the details
				if(!obj.get("ts").toString().equals("null"))
				{
					JSONArray trips = obj.getJSONArray("ts");
					setTripInfo(trips, tripDetails, i, j);
				}
			}
	}
	
	public String getJson(String url) throws IOException
	{
		// connect to url
		HttpURLConnection c = null;

		URL u = new URL(url);
		c = (HttpURLConnection) u.openConnection();
		c.setRequestMethod("GET");
		c.setRequestProperty("Content-length", "0");
		c.setUseCaches(false);
		c.setAllowUserInteraction(false);
		c.connect();
		int status = c.getResponseCode();

		switch (status) {
		case 200:
		case 201:
			BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			return sb.toString();
			
		default:
			throw new IOException("Http Error: " + status);
		}
	}
	
	public List<TripInformationsType> findRoute(int modes, String session, TripInformationsType[][] tripDetails, List<PoiType> poiList) throws IOException
	{
	    	int [] minDurations = new int[poiList.size()];
			int [] visited = new int[poiList.size()];
			int minCurrent;
			int lastVisitedIndex = 0;
			boolean minFlag;
			
			//used for check if car is leaved
			boolean leaveCar = false;
			//The stack with the indexes of the pois
			Stack<Integer> poiIndexes = new Stack<Integer>();
			//push index of first poi in the top of stack
			poiIndexes.push(0);
			
			List<TripInformationsType> tripInformations = new ArrayList<TripInformationsType>();
			
			while(!poiIndexes.isEmpty())
			{
				minCurrent = Integer.MAX_VALUE;
				minFlag = false;
				
				//pick current poi (without remove it)
				int current = poiIndexes.peek();
				
				for (PoiType p : poiList)
				{
					//if it's a Car trip and the car is no more available, skip the trip
					if(!tripDetails[current][poiList.indexOf(p)].getSegments().isEmpty())
					{
						if(tripDetails[current][poiList.indexOf(p)].getSegments().get(0).getType() == "Car" && leaveCar)
						{
							continue;
						}
					}
					
					if(tripDetails[current][poiList.indexOf(p)].getDuration() < minCurrent && (current != poiList.indexOf(p))
							&& (visited[poiList.indexOf(p)] == 0))
					{
						minCurrent = tripDetails[current][poiList.indexOf(p)].getDuration();
						lastVisitedIndex = poiList.indexOf(p);
						minFlag = true;
					}
				}
				
				//set minimum duration for currentPoi
				minDurations[current] = minCurrent;
				//set current as visited
				visited[current] = 1;
				//remove current from the stack
				poiIndexes.pop();
				
				if(minFlag)
				{
					//the next poi that will be visited
					poiIndexes.push(lastVisitedIndex);
					
					//if the trip is not empty
					if(tripDetails[current][lastVisitedIndex].getSegments().size() > 0)
					{
						//if the type of this trip is not Car, the car will be not used anymore as transport type in the route
						if(tripDetails[current][lastVisitedIndex].getSegments().get(0).getType() != "Car")
						{
							leaveCar = true;
						}
						
						//save informations/segments 
						TripInformationsType t = getTripInfo(poiList, current, lastVisitedIndex, session, tripDetails);
						
						if(t != null)
						{
							tripInformations.add(t);
						}
					}
				}
			}
			
			//put the duration from last poi to starting poi
			minDurations[lastVisitedIndex] = tripDetails[lastVisitedIndex][0].getDuration();
			//build the last tripInformation, from last poi to starting poi
			TripInformationsType t = getTripInfo(poiList, lastVisitedIndex, 0, session, tripDetails);
			tripInformations.add(t);
			
			return tripInformations;
	 }
	
	public TripInformationsType getTripInfo(List<PoiType> poiList, int from, int to, String session, TripInformationsType[][] tripDetails) throws IOException
	{
		TripInformationsType t = new TripInformationsType();
		//add poi names
		t.setFromPoiName(poiList.get(from).getName());
		t.setToPoiName(poiList.get(to).getName());
		//add segments of the trip
		if(!tripDetails[from][to].getSegments().isEmpty())
		{
			t.getSegments().addAll(tripDetails[from][to].getSegments());
		}
		//add duration
		t.setDuration(tripDetails[from][to].getDuration());
		return t;
	 }
}

package it.softeco.chorevolution.smt.journeyplanner.business;

import it.softeco.chorevolution.smt.journeyplanner.AllTripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.AllTripsResponse;

public interface JourneyPlannerService {

	AllTripsResponse allTrips(AllTripsRequest parameters) throws BusinessException;
	
}

package it.softeco.chorevolution.smt.googledirections.business;

import it.softeco.chorevolution.smt.googledirections.AllTripsRequest;
import it.softeco.chorevolution.smt.googledirections.AllTripsResponse;

public interface GoogleDirectionsService {
	
	AllTripsResponse allTrips(AllTripsRequest parameters) throws BusinessException;
	
}

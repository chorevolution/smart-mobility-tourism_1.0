package it.softeco.chorevolution.smt.googledirections.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.googledirections.AllTripsRequest;
import it.softeco.chorevolution.smt.googledirections.AllTripsResponse;
import it.softeco.chorevolution.smt.googledirections.GoogleDirectionsPT;
import it.softeco.chorevolution.smt.googledirections.business.BusinessException;
import it.softeco.chorevolution.smt.googledirections.business.GoogleDirectionsService;

@Component(value = "GoogleDirectionsPTImpl")
public class GoogleDirectionsPTImpl implements GoogleDirectionsPT
{
	private static Logger logger = LoggerFactory.getLogger(GoogleDirectionsPTImpl.class);
	
	@Autowired
	private GoogleDirectionsService service;
	
	@Override
	public AllTripsResponse allTrips(AllTripsRequest parameters)
	{
		logger.info("CALLED allTrips ON googledirections");
		
		try
		{
			AllTripsResponse resp = service.allTrips(parameters);
			return resp;
		}
		catch (BusinessException ex) {
            logger.error("error", ex);
    		throw new RuntimeException(ex.getMessage());
        }
	}
}

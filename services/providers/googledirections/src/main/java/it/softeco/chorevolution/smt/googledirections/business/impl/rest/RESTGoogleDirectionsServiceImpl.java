package it.softeco.chorevolution.smt.googledirections.business.impl.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.googledirections.AllTripsRequest;
import it.softeco.chorevolution.smt.googledirections.AllTripsResponse;
import it.softeco.chorevolution.smt.googledirections.BusDetailsType;
import it.softeco.chorevolution.smt.googledirections.PoiType;
import it.softeco.chorevolution.smt.googledirections.PositionType;
import it.softeco.chorevolution.smt.googledirections.SegmentType;
import it.softeco.chorevolution.smt.googledirections.TripInformationsType;
import it.softeco.chorevolution.smt.googledirections.business.BusinessException;
import it.softeco.chorevolution.smt.googledirections.business.GoogleDirectionsService;


@Service
public class RESTGoogleDirectionsServiceImpl implements GoogleDirectionsService {

	private static Logger logger = LoggerFactory.getLogger(RESTGoogleDirectionsServiceImpl.class);

	@Value("#{cfgproperties.googledirectionsurl}")
	private String googleUrl;

	@Override
	public AllTripsResponse allTrips(AllTripsRequest googleRequest) throws BusinessException {
		
		//put the starting poi at the beginning of the poiList
		PoiType startingPoi = new PoiType();
		startingPoi.setLat(googleRequest.getFromLat());
		startingPoi.setLon(googleRequest.getFromLon());
		startingPoi.setName("Current Position");
		googleRequest.getPoiList().add(0, startingPoi);
		
		//the object containing the durations and the details of the trips
		TripInformationsType[][] tripDetails = new TripInformationsType[googleRequest.getPoiList().size()][googleRequest.getPoiList().size()];
		
		//initialize matrix of informations
		for(int i=0; i<googleRequest.getPoiList().size(); i++)
		{
			for(int j=0; j<googleRequest.getPoiList().size(); j++)
			{
				tripDetails[i][j]= new TripInformationsType();
			}
		}
		
		//fill the matrix
		for (int i = 0; i < googleRequest.getPoiList().size(); i++) 
		{
			for (int j = 0; j < googleRequest.getPoiList().size(); j++)
			{
				try {
					fillMatrix(i, j, googleRequest.getPoiList(), googleRequest.getMode(), googleRequest.getSession(), tripDetails);
				} 
				catch (IOException e) {
					throw new BusinessException(e.getMessage());
				}
			}
		}
		
		//find the best route
    	try 
    	{
			List<TripInformationsType> trips = findRoute(googleRequest.getMode(), googleRequest.getSession(), tripDetails, googleRequest.getPoiList());
			AllTripsResponse resp = new AllTripsResponse();
			resp.getTrips().addAll(trips);
			return resp;
		}
    	catch (IOException e) {
    		throw new BusinessException(e.getMessage());
		}
	}
	
	public void fillMatrix(int i, int j, List<PoiType> poiList, String mode, String session, TripInformationsType[][] tripDetails) throws IOException
	{
			//if poi with itself, duration is 0
			if (i == j) 
			{
				tripDetails[i][j].setDuration(0); 
			}
			else 
			{
				//build the url
				String url = buildUrl(poiList, session, mode, i, j);
				//get json response from url
				String json;
				json = getJson(url); 
				//parse the json response and save the duration of the trip in the matrix
				JSONObject obj = new JSONObject(json);
				
				//if no trips found with car transport type, try to search for bus
				if(obj.get("routes").toString().equals("null") && mode=="driving")
				{
					fillMatrix(i, j, poiList, "transit", session, tripDetails);
					return;
				}
				//if trips found, save the duration and the details
				if(!obj.get("routes").toString().equals("null"))
				{
					JSONArray trip = obj.getJSONArray("routes");
					setTripInfo(trip, tripDetails, i, j, mode);
				}
			}
	}
	
	public String buildUrl(List<PoiType> poiList, String session, String mode, int from, int to)
	{
		 return googleUrl +
				"&origin=" + poiList.get(from).getLat()+","+poiList.get(from).getLon() +
				"&destination=" + poiList.get(to).getLat()+","+poiList.get(to).getLon() +
				"&mode=" + mode; 
	}
	
	public String getJson(String url) throws IOException
	{
		// connect to url
		HttpURLConnection c = null;

		URL u = new URL(url);
		c = (HttpURLConnection) u.openConnection();
		c.setRequestMethod("GET");
		c.setRequestProperty("Content-length", "0");
		c.setUseCaches(false);
		c.setAllowUserInteraction(false);
		c.connect();
		int status = c.getResponseCode();

		switch (status) {
		case 200:
		case 201:
			BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			return sb.toString();
			
		default:
			throw new IOException("Http Error: " + status);
		}
	}
	
	public void setTripInfo(JSONArray trip, TripInformationsType[][] tripDetails, int i, int j, String mode)
	{
		//pick the first element of trip array
		JSONObject route = trip.getJSONObject(0);
		//pick the first element of legs array
		JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
		//save duration field
		tripDetails[i][j].setDuration(leg.getJSONObject("duration").getInt("value")); 
		//pick segments in steps array
		JSONArray segs = leg.getJSONArray("steps");

		//if the trip is by walk or by car, build one segment with all the coordinates
		if(mode.equalsIgnoreCase("driving") || mode.equalsIgnoreCase("walking"))
		{
			List<PositionType> globalCoords = new ArrayList<PositionType>();
			//iterate segments and pick the coords
			for (int y=0; y<segs.length(); y++)
			{
				JSONObject current = segs.getJSONObject(y);
				//save coordinates of the segment in the global list
				String poly = current.getJSONObject("polyline").getString("points");
				List<PositionType> segmentCoords = decodePoly(poly);
				globalCoords.addAll(segmentCoords);
			}
			//build the segment
			SegmentType s = new SegmentType();
			String type = (mode.equalsIgnoreCase("driving")) ? "Car" : "Walk";
			s.setType(type);
			s.getPositions().addAll(globalCoords);
			//add it to trip details
			tripDetails[i][j].getSegments().add(s);
		}
		
		//bus mode
		if(mode.equalsIgnoreCase("transit"))
		{
			List<SegmentType> segments = new ArrayList<SegmentType>();
			//iterate array of steps only if by bus
			for (int y=0; y<segs.length(); y++)
			{
				JSONObject current = segs.getJSONObject(y);
				SegmentType s = new SegmentType();
				//set type of the segment
				switch (current.getString("travel_mode"))
				{
					case "WALKING":
						s.setType("Walk");
						break;
					case "TRANSIT":
						s.setType("Bus");
						break;
					default:
						break;
				}
				
				//set coordinates of the segment
				String poly = current.getJSONObject("polyline").getString("points");
				List<PositionType> coords = decodePoly(poly);
				s.getPositions().addAll(coords);
				
				//build the busDetails(if present)
				if(!current.isNull("transit_details"))
				{
					BusDetailsType busDetails = new BusDetailsType();
					JSONObject bus = current.getJSONObject("transit_details");
					busDetails.setDirection(bus.getString("headsign"));
					busDetails.setDropInNode(bus.getJSONObject("departure_stop").getString("name"));
					busDetails.setDropOffNode(bus.getJSONObject("arrival_stop").getString("name"));
					busDetails.setLine(bus.getJSONObject("line").getString("short_name"));
					s.setBusDetails(busDetails);
				}
				segments.add(s);
			}
			//add details
			tripDetails[i][j].getSegments().addAll(segments);
		}
	}
	
	public List<TripInformationsType> findRoute(String mode, String session, TripInformationsType[][] tripDetails, List<PoiType> poiList) throws IOException
	{
	    	int [] minDurations = new int[poiList.size()];
			int [] visited = new int[poiList.size()];
			int minCurrent;
			int lastVisitedIndex = 0;
			boolean minFlag;
			
			//used for check if car is leaved
			boolean leaveCar = false;
			//The stack with the indexes of the pois
			Stack<Integer> poiIndexes = new Stack<Integer>();
			//push index of first poi in the top of stack
			poiIndexes.push(0);
			
			List<TripInformationsType> tripInformations = new ArrayList<TripInformationsType>();
			
			while(!poiIndexes.isEmpty())
			{
				minCurrent = Integer.MAX_VALUE;
				minFlag = false;
				
				//pick current poi (without remove it)
				int current = poiIndexes.peek();
				
				for (PoiType p : poiList)
				{
					//if it's a Car trip and the car is no more available, skip the trip
					if(!tripDetails[current][poiList.indexOf(p)].getSegments().isEmpty())
					{
						if(tripDetails[current][poiList.indexOf(p)].getSegments().get(0).getType() == "Car" && leaveCar)
						{
							continue;
						}
					}
					
					if(tripDetails[current][poiList.indexOf(p)].getDuration() < minCurrent && (current != poiList.indexOf(p))
							&& (visited[poiList.indexOf(p)] == 0))
					{
						minCurrent = tripDetails[current][poiList.indexOf(p)].getDuration();
						lastVisitedIndex = poiList.indexOf(p);
						minFlag = true;
					}
				}
				
				//set minimum duration for currentPoi
				minDurations[current] = minCurrent;
				//set current as visited
				visited[current] = 1;
				//remove current from the stack
				poiIndexes.pop();
				
				if(minFlag)
				{
					//the next poi that will be visited
					poiIndexes.push(lastVisitedIndex);
					
					//if the type of this trip is not Car, the car will be not used anymore as transport type in the route
					if(tripDetails[current][lastVisitedIndex].getSegments().get(0).getType() != "Car")
					{
						leaveCar = true;
					}
					
					//save informations/segments 
					TripInformationsType t = getTripInfo(poiList, current, lastVisitedIndex, session, tripDetails);
					
					if(t != null)
					{
						tripInformations.add(t);
					}
				}
			}
			
			//put the duration from last poi to starting poi
			minDurations[lastVisitedIndex] = tripDetails[lastVisitedIndex][0].getDuration();
			//build the last tripInformation, from last poi to starting poi
			TripInformationsType t = getTripInfo(poiList, lastVisitedIndex, 0, session, tripDetails);
			tripInformations.add(t);
			
			return tripInformations;
	 }
	
	public TripInformationsType getTripInfo(List<PoiType> poiList, int from, int to, String session, TripInformationsType[][] tripDetails) throws IOException
	{
		TripInformationsType t = new TripInformationsType();
		//add poi names
		t.setFromPoiName(poiList.get(from).getName());
		t.setToPoiName(poiList.get(to).getName());
		//add segments of the trip
		if(!tripDetails[from][to].getSegments().isEmpty())
		{
			t.getSegments().addAll(tripDetails[from][to].getSegments());
		}
		//add duration
		t.setDuration(tripDetails[from][to].getDuration());
		return t;
	 }

	//decode google directions polyline
	public static List<PositionType> decodePoly(String encoded) 
	{
		List<PositionType> poly = new ArrayList<PositionType>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;
		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;
			PositionType p = new PositionType();
			p.setLat((double) lat / 1E5);
			p.setLon((double) lng / 1E5);
			poly.add(p);
		}
		return poly;
	}
}
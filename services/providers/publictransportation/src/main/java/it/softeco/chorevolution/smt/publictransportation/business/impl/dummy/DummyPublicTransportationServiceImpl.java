package it.softeco.chorevolution.smt.publictransportation.business.impl.dummy;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.publictransportation.InfoItemType;
import it.softeco.chorevolution.smt.publictransportation.business.BusinessException;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Service
public class DummyPublicTransportationServiceImpl implements PublicTransportationService {

	@Override
	public List<InfoItemType> getTransportInfo() throws BusinessException {
		List<InfoItemType>  response = new ArrayList<>();
		InfoItemType element = new InfoItemType();
		element.setText("test-");
		response.add(element);		
		return response;
	}

	
}

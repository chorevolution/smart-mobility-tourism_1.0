package it.softeco.chorevolution.smt.publictransportation.business;

import java.util.List;

import it.softeco.chorevolution.smt.publictransportation.InfoItemType;

public interface PublicTransportationService {

	List<InfoItemType> getTransportInfo() throws BusinessException;
}

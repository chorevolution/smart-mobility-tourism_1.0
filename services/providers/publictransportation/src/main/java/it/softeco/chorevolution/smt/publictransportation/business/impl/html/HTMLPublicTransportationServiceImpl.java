package it.softeco.chorevolution.smt.publictransportation.business.impl.html;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.publictransportation.InfoItemType;
import it.softeco.chorevolution.smt.publictransportation.business.BusinessException;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Service
public class HTMLPublicTransportationServiceImpl implements PublicTransportationService {
	
	private static Logger logger = LoggerFactory.getLogger(HTMLPublicTransportationServiceImpl.class);
	@Value("#{cfgproperties.htmlPage}")
	private String htmlPage;
	
	@Override
	public List<InfoItemType> getTransportInfo() throws BusinessException 
	{
		try {
			//get html page
			Document d = Jsoup.connect(htmlPage).get();
			//get div of news
			Elements div = d.select("div#recent-posts-2");
			//get li elements
			Elements li = div.select("li");
			
			List<InfoItemType> response = new ArrayList<InfoItemType>();
		    for (Element el : li) 
		    {
		    	InfoItemType t = new InfoItemType();
		    	//get the title
		    	String title = el.text().replaceAll("\\d{2}/\\d{2}/\\d{4}", "");
		    	t.setTitle(title);
		    	//get the link for detailed info, read the page and save the containing description
		    	Elements link = el.select("a[href]");
		    	Document linksDoc = Jsoup.connect(link.attr("href")).get();
		    	Elements divDetails = linksDoc.select("div[class=post_content]");
		    	t.setText(divDetails.text());
		    	//get the date
		    	Elements dateDetails = linksDoc.select("div[class=post_date]");
		    	t.setIssueDate(dateDetails.text());
		    	
		    	response.add(t);
		    }
		    return response;
		}
		catch (Exception e) 
		{
			logger.error(e.getMessage(), e);
			throw new BusinessException(e);
		}
	}
}
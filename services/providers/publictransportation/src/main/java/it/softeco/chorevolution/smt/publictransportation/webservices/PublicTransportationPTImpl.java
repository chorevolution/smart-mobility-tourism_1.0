package it.softeco.chorevolution.smt.publictransportation.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.publictransportation.GetInfoTransportationRequest;
import it.softeco.chorevolution.smt.publictransportation.GetInfoTransportationResponse;
import it.softeco.chorevolution.smt.publictransportation.PublicTransportationPT;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Component(value = "PublicTransportationPTImpl")
public class PublicTransportationPTImpl implements PublicTransportationPT {

	private static Logger logger = LoggerFactory.getLogger(PublicTransportationPTImpl.class);
	
	@Autowired
	private PublicTransportationService service;
	

	@Override
	public GetInfoTransportationResponse getInfo(GetInfoTransportationRequest parameters) {

		logger.info("CALLED getInfo ON PublicTransportationInformation");

		try {

			GetInfoTransportationResponse info = new GetInfoTransportationResponse();
			info.getInfoItem().addAll(service.getTransportInfo());
			return info;
		} 
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}

}

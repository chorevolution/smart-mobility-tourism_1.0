package it.softeco.chorevolution.smt.osmparking.business.impl.rest;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.softeco.chorevolution.smt.osmparking.OSMParkingType;
import it.softeco.chorevolution.smt.osmparking.ParkingInfoType;
import it.softeco.chorevolution.smt.osmparking.ParkingResponse;
import it.softeco.chorevolution.smt.osmparking.business.BusinessException;
import it.softeco.chorevolution.smt.osmparking.business.OSMParkingService;

@Service
public class RESTOSMParkingServiceImpl implements OSMParkingService {

	private static Logger logger = LoggerFactory.getLogger(RESTOSMParkingServiceImpl.class);
	
	@Value("#{cfgproperties.overpass_api}")
	private String OVERPASS_API;
	
	private static final double km2degree = 0.0089982311916;
	
	@Override
	public ParkingResponse getParking(Double lat, Double lon, Integer vicinityRange) throws BusinessException {
		Document xml;
		try {
			xml = getXmlResponse(lat, lon, vicinityRange);
			List<OSMNode> nodes = getNodes(xml);
			ParkingResponse response = getParkingInfo(nodes);
			return response;
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}

	// get response xml
	private Document getXmlResponse(Double lat, Double lon, Integer vicinityRange) throws Exception {

		if (lat == null || lat < -90 || lat > 90)
			throw new IllegalArgumentException("Lat must be a value between -90 and +90");

		if (lon == null || lon < -180 || lon > 180)
			throw new IllegalArgumentException("Lon must be a value between -180 and +180");

		if (vicinityRange == null || vicinityRange < 1)
			throw new IllegalArgumentException("Range must be an integer > 1");

		// convert m to km
		double vicinityRangeToKM = (double) vicinityRange / 1000;

		// decimal precision
		DecimalFormat format = new DecimalFormat("##0.0000000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

		String left = format.format(lon - (vicinityRangeToKM * km2degree));
		String right = format.format(lon + (vicinityRangeToKM * km2degree));
		String bottom = format.format(lat - (vicinityRangeToKM * km2degree));
		String top = format.format(lat + (vicinityRangeToKM * km2degree));

		String url = OVERPASS_API + "?data=node(" + bottom + "," + left + "," + top + "," + right + ")" + "[amenity=parking]" + ";out;";

		URL overpassUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) overpassUrl.openConnection();

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		return docBuilder.parse(connection.getInputStream());
	}

	// create POIs results list
	private List<OSMNode> getNodes(Document xmlDocument) throws IllegalArgumentException {

		List<OSMNode> osmNodes = new ArrayList<OSMNode>();

		Node osmRoot = xmlDocument.getFirstChild();
		NodeList osmXMLNodes = osmRoot.getChildNodes();
		for (int i = 1; i < osmXMLNodes.getLength(); i++) {
			Node item = osmXMLNodes.item(i);
			if (item.getNodeName().equals("node")) {
				NamedNodeMap attributes = item.getAttributes();
				NodeList tagXMLNodes = item.getChildNodes();
				Map<String, String> tags = new HashMap<String, String>();
				for (int j = 1; j < tagXMLNodes.getLength(); j++) {
					Node tagItem = tagXMLNodes.item(j);
					NamedNodeMap tagAttributes = tagItem.getAttributes();
					if (tagAttributes != null) {
						tags.put(tagAttributes.getNamedItem("k").getNodeValue(), tagAttributes.getNamedItem("v").getNodeValue());
					}
				}
				Node namedItemID = attributes.getNamedItem("id");
				Node namedItemLat = attributes.getNamedItem("lat");
				Node namedItemLon = attributes.getNamedItem("lon");
				Node namedItemVersion = attributes.getNamedItem("version");

				String id = namedItemID.getNodeValue();
				String latitude = namedItemLat.getNodeValue();
				String longitude = namedItemLon.getNodeValue();
				String version = "0";
				if (namedItemVersion != null) {
					version = namedItemVersion.getNodeValue();
				}
				osmNodes.add(new OSMNode(id, latitude, longitude, version, tags));
			}
		}

		if (osmNodes.isEmpty()) {
			throw new IllegalArgumentException("Research provided no results");
		}
		return osmNodes;
	}

	private ParkingResponse getParkingInfo(List<OSMNode> l) throws Exception {

		List<OSMParkingType> results = new ArrayList<OSMParkingType>();

		for (OSMNode osmNode : l) {

			// create OSMParkingType
			OSMParkingType node = new OSMParkingType();

			try {
				// lat,lon
				node.setLat(Double.parseDouble(osmNode.getLat()));
				node.setLon(Double.parseDouble(osmNode.getLon()));

				// parkingInfo
				ParkingInfoType info = null;

				if (osmNode.getTags().get("name") != null) {
					info = new ParkingInfoType();
					info.setName(osmNode.getTags().get("name"));
				}
				if (osmNode.getTags().get("capacity") != null) {
					if (info == null)
						info = new ParkingInfoType();

					info.setCapacity(Integer.parseInt(osmNode.getTags().get("capacity")));
				}
				if (osmNode.getTags().get("capacity:disabled") != null) {
					if (info == null)
						info = new ParkingInfoType();

					info.setCapacityDisabled(Integer.parseInt(osmNode.getTags().get("capacity:disabled")));
				}
				if (osmNode.getTags().get("fee") != null) {
					if (info == null)
						info = new ParkingInfoType();

					info.setFee(osmNode.getTags().get("fee"));
				}
				if (info != null)
					node.setParkingInfo(info);

				// add node to list
				results.add(node);
			}

			catch (Exception e) {
				throw new Exception(e);
			}
		}

		ParkingResponse response = new ParkingResponse();
		response.getParkings().addAll(results);
		return response;
	}

}

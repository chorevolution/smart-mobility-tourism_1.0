package it.softeco.chorevolution.smt.osmparking.business;

import it.softeco.chorevolution.smt.osmparking.ParkingResponse;

public interface OSMParkingService {

	ParkingResponse getParking(Double lat, Double lon, Integer vicinityRange) throws BusinessException;
}

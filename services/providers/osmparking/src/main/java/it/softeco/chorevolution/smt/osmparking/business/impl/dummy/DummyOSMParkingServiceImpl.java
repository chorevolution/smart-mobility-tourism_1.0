package it.softeco.chorevolution.smt.osmparking.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.osmparking.ParkingResponse;
import it.softeco.chorevolution.smt.osmparking.business.BusinessException;
import it.softeco.chorevolution.smt.osmparking.business.OSMParkingService;

@Service
public class DummyOSMParkingServiceImpl implements OSMParkingService {
	
	@Override
	public ParkingResponse getParking(Double lat, Double lon, Integer vicinityRange) throws BusinessException {
		ParkingResponse response = new ParkingResponse();
		return response;
	}
	
	

}

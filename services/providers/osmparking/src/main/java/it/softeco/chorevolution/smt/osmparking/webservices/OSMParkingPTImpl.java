package it.softeco.chorevolution.smt.osmparking.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.osmparking.OSMParkingPT;
import it.softeco.chorevolution.smt.osmparking.ParkingRequest;
import it.softeco.chorevolution.smt.osmparking.ParkingResponse;
import it.softeco.chorevolution.smt.osmparking.business.OSMParkingService;

@Component(value = "OSMParkingPTImpl")
public class OSMParkingPTImpl implements OSMParkingPT {

	private static Logger logger = LoggerFactory.getLogger(OSMParkingPTImpl.class);
	
	@Autowired
	private OSMParkingService service;

	public ParkingResponse parking(ParkingRequest parameters) {

		logger.info("CALLED parking ON OSMPARKING");

		try {
			ParkingResponse response = service.getParking(parameters.getLat(), parameters.getLon(), parameters.getRange());
			return response;
		} 
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}
}

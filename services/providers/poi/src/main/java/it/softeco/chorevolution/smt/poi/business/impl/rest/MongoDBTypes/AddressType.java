package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class AddressType 
{
    private String street;
    private String houseNumber;
    private String phone;

    public String getStreet() {
        return street;
    }

    public void setStreet(String value) {
        this.street = value;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String value) {
        this.houseNumber = value;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String value) {
        this.phone = value;
    }
}

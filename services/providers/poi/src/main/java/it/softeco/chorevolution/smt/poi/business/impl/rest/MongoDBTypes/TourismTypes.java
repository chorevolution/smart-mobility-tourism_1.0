package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import java.util.Arrays;
import java.util.List;

public class TourismTypes 
{
	public static List<String> types = Arrays.asList("cinema","hotel","castle","acquarium","museum","viewpoint","zoo");
}

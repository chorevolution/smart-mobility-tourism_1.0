package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.utils.IndexType;
import org.mongodb.morphia.annotations.Field;


//pois will be the collection name on MongoDB
@Entity(value="pois", noClassnameStored = true)
@Indexes({
	@Index(fields = @Field(value = "position", type = IndexType.GEO2D)),
	@Index(fields = { @Field("name"), @Field("position")}, options = @IndexOptions(unique = true))
})
public class Poi
{

	@Id
    private ObjectId id;
	private double[] position;
	private String type;
	private String name;
	private PoiInfo poiInfo;
	
	public double[] getPosition() {
		return position;
	}
	public void setPosition(double[] position) {
		this.position = position;
	}
	public PoiInfo getPoiInfo() {
		return poiInfo;
	}
	public void setPoiInfo(PoiInfo poiInfo) {
		this.poiInfo = poiInfo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}

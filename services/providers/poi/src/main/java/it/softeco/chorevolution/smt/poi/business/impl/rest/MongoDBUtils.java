package it.softeco.chorevolution.smt.poi.business.impl.rest;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.Shape;
import org.mongodb.morphia.query.Shape.Point;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;

import it.softeco.chorevolution.smt.poi.Tags;
import it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes.Poi;
import it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes.PoiType;
import it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes.TourismTypes;

public class MongoDBUtils 
{
	public void storePois(String poiList)
	{
		try
		{
			//convert json passed to java poiType list
			JsonParser parser = new JsonParser();
	    	JsonObject obj = parser.parse(poiList).getAsJsonObject();
	    	Type listType = new TypeToken<ArrayList<PoiType>>(){}.getType();
	    	ArrayList<PoiType> pois = new Gson().fromJson(obj.get("poi"), listType);
	    	//create mongo instance
	    	MongoClient mongoClient = new MongoClient("172.16.1.13", 27017);
			//create morphia instance
			final Morphia morphia = new Morphia();
			//search for annotated classes
			morphia.map(Poi.class);
			// create the Datastore connecting to the default port on the local host at given database
			final Datastore datastore = morphia.createDatastore(mongoClient, "chorevolution");
			datastore.ensureIndexes();
			//add the passed poilist in db "test", in the collection "pois", like annotated in poi class
			for (PoiType poi : pois)
			{
				try
				{
					//build poi object
					Poi p = new Poi();
					p.setName(poi.getName());
					p.setPoiInfo(poi.getPoiInfo());
					p.setType(poi.getType());
					double[] position = new double[2];
					position[0]=poi.getLon();
					position[1]=poi.getLat();
					p.setPosition(position);
					//save poi in the db
					datastore.save(p);
				}
				catch(DuplicateKeyException e)
				{
					continue;
				}
				catch (Exception e)
				{
					throw new RuntimeException(e.getMessage());
				}
			}
			//release resource
			mongoClient.close();
		}
		catch (Exception e)
	    {
	        throw new RuntimeException(e.getMessage());
	    }
	}
	public String retrievePois(double lat, double lon, int vicinityRange, List<Tags> tags)
	{
		try
		{
			//create mongo instance
	    	MongoClient mongoClient = new MongoClient("172.16.1.13", 27017);
			//create morphia instance
			final Morphia morphia = new Morphia();
			//search for annotated classes
			morphia.map(Poi.class);
			// create the Datastore connecting to the default port on the local host at given database
			final Datastore datastore = morphia.createDatastore(mongoClient, "chorevolution");
			datastore.ensureIndexes();
			
			//find if there are pois in the given bounding box
			double km2degree = 0.0089982311916;
			double vicinityRangeToKM = (double) vicinityRange / 1000;
			DecimalFormat format = new DecimalFormat("##0.0000000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
			String left = format.format(lon - (vicinityRangeToKM * km2degree));
			String right = format.format(lon + (vicinityRangeToKM * km2degree));
			String bottom = format.format(lat - (vicinityRangeToKM * km2degree));
			String top = format.format(lat + (vicinityRangeToKM * km2degree));
			
			//geospatial query parameters
			Point bottomLeft = new Point(Double.parseDouble(left), Double.parseDouble(bottom));
			Point upperRight = new Point(Double.parseDouble(right), Double.parseDouble(top));
			Query<Poi> q;
			
			//pick all tag values for the query
			List<String> types = new ArrayList<String>();
			for (Tags tag : tags)
			{
				if(tag.value().equals("tourism"))
				{
					List<String> temp = TourismTypes.types;
					types.addAll(temp);
				}
				else
				{
					String value = tag.value();
					types.add(value);
				}
			}
			
			//build the query
			q = datastore.createQuery(Poi.class).field("position").within(Shape.box(bottomLeft, upperRight)).field("type").in(types);
			
			//save and return found pois
			List<Poi> l = q.asList();
			List<PoiType> pois = new ArrayList<PoiType>();
			for (Poi p : l)
			{
				PoiType poi = new PoiType();
				poi.setName(p.getName());
				poi.setType(p.getType());
				poi.setPoiInfo(p.getPoiInfo());
				poi.setLat(p.getPosition()[1]);
				poi.setLon(p.getPosition()[0]);
				pois.add(poi);
			}
			Gson gson = new Gson();
			String jsonInString = gson.toJson(pois);
			//release resource
			mongoClient.close();
			return jsonInString;
		}
		catch(Exception e)
		{
			throw new RuntimeException(e.getMessage());
		}
	}
}
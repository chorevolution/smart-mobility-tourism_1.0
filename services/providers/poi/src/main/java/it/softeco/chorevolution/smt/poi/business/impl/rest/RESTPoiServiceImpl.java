package it.softeco.chorevolution.smt.poi.business.impl.rest;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.softeco.chorevolution.smt.poi.AddressType;
import it.softeco.chorevolution.smt.poi.BusInfoType;
import it.softeco.chorevolution.smt.poi.GetPoiListResponse;
import it.softeco.chorevolution.smt.poi.PoiInfo;
import it.softeco.chorevolution.smt.poi.PoiType;
import it.softeco.chorevolution.smt.poi.ReligionType;
import it.softeco.chorevolution.smt.poi.Tags;
import it.softeco.chorevolution.smt.poi.business.BusinessException;
import it.softeco.chorevolution.smt.poi.business.PoiService;

@Service
public class RESTPoiServiceImpl implements PoiService {
	
	private static Logger logger = LoggerFactory.getLogger(RESTPoiServiceImpl.class);
	
	@Value("#{cfgproperties.overpass_api}")
	private String OVERPASS_API;
	private static final double km2degree = 0.0089982311916;

	@Override
	public GetPoiListResponse getPoiList(Double lat, Double lon, Integer vicinityRange, List<Tags> tags, String session) throws BusinessException {
		Document xml;
		try {
			xml = getXmlResponse(lat, lon, vicinityRange, tags);
			List<OSMNode> nodes = getNodes(xml);
			GetPoiListResponse response = getPoiInfo(nodes);
			response.setSession(session);
			//convert the poiList to json to store found results on MongoDB
			/*Gson gson = new Gson();
			String jsonPoiList = gson.toJson(response);
			MongoDBUtils mongo = new MongoDBUtils();
			mongo.storePois(jsonPoiList);*/
		    //return poiList
			return response;
		}
		//if timeout occurs, search saved pois in MongoDB instead 
		/*catch(IOException e)
		{ 
			try
			{
				MongoDBUtils mongo = new MongoDBUtils();
				String jsonPoiList = mongo.retrievePois(lat, lon, vicinityRange, tags);
				Gson gson = new Gson();
				Type collectionType = new TypeToken<List<PoiType>>(){}.getType();
				List<PoiType> poiList = gson.fromJson(jsonPoiList, collectionType);
				GetPoiListResponse response = new GetPoiListResponse();
				response.getPoi().addAll(poiList);
				response.setSession(session);
				return response;
			} 
			catch(Exception ex)
			{
				throw new BusinessException();
			}
		}*/
		catch (Exception e)
		{
			logger.error("error", e);
			throw new BusinessException();
		}
	}
	
	public enum keys {
		worship, restaurant, school, bus, hospital, tourism
	}

	// get response xml from url calculated
	private Document getXmlResponse(Double lat, Double lon, Integer vicinityRange, List<Tags> key) throws Exception {

		if (lat == null || lat < -90 || lat > 90)
			throw new IllegalArgumentException("Lat must be a value between -90 and +90");

		if (lon == null || lon < -180 || lon > 180)
			throw new IllegalArgumentException("Lon must be a value between -180 and +180");

		if (vicinityRange == null || vicinityRange < 1)
			throw new IllegalArgumentException("Range must be an integer > 1");

		if (key == null)
			throw new IllegalArgumentException(
					"Tag must be one the following values: " + java.util.Arrays.asList(keys.values()));

		// convert m to km
		double vicinityRangeToKM = (double) vicinityRange / 1000;

		// decimal precision
		DecimalFormat format = new DecimalFormat("##0.0000000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		String left = format.format(lon - (vicinityRangeToKM * km2degree));
		String right = format.format(lon + (vicinityRangeToKM * km2degree));
		String bottom = format.format(lat - (vicinityRangeToKM * km2degree));
		String top = format.format(lat + (vicinityRangeToKM * km2degree));

		String url = OVERPASS_API + "?data=(";
		//iterate all tags and create the url for OpenStreetMap
		for (Tags tag : key)
		{
			switch (tag.value())
			{
				case "worship":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[amenity=place_of_worship];");
					break;
					
				case "restaurant":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[amenity=restaurant];");
					break;
			
				case "school":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[amenity=school];");
					break;
				
				case "bus":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[highway=bus_stop];");
					break;
					
				case "hospital":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[amenity=hospital];");
					break;
					
				case "tourism":
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[amenity=cinema];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[tourism=hotel];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[historic=castle];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[tourism=acquarium];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[tourism=museum];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[tourism=viewpoint];");
					url = url.concat("node(" + bottom + "," + left + "," + top + "," + right + ")"
					+ "[tourism=zoo];");
					break;
	
				default:
					throw new IllegalArgumentException("Tag must be one the following values: " + java.util.Arrays.asList(keys.values()));
			}
		}
		
		//add final url part
		url = url.concat(");out;");

		URL overpassUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) overpassUrl.openConnection();

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		return docBuilder.parse(connection.getInputStream());
	}

	// create POIs results list
	private List<OSMNode> getNodes(Document xmlDocument) throws Exception {

		List<OSMNode> osmNodes = new ArrayList<OSMNode>();

		Node osmRoot = xmlDocument.getFirstChild();
		NodeList osmXMLNodes = osmRoot.getChildNodes();
		for (int i = 1; i < osmXMLNodes.getLength(); i++) {
			Node item = osmXMLNodes.item(i);
			if (item.getNodeName().equals("node")) {
				NamedNodeMap attributes = item.getAttributes();
				NodeList tagXMLNodes = item.getChildNodes();
				Map<String, String> tags = new HashMap<String, String>();
				for (int j = 1; j < tagXMLNodes.getLength(); j++) {
					Node tagItem = tagXMLNodes.item(j);
					NamedNodeMap tagAttributes = tagItem.getAttributes();
					if (tagAttributes != null) {
						tags.put(tagAttributes.getNamedItem("k").getNodeValue(),
								tagAttributes.getNamedItem("v").getNodeValue());
					}
				}
				Node namedItemID = attributes.getNamedItem("id");
				Node namedItemLat = attributes.getNamedItem("lat");
				Node namedItemLon = attributes.getNamedItem("lon");
				Node namedItemVersion = attributes.getNamedItem("version");

				String id = namedItemID.getNodeValue();
				String latitude = namedItemLat.getNodeValue();
				String longitude = namedItemLon.getNodeValue();
				String version = "0";
				if (namedItemVersion != null) {
					version = namedItemVersion.getNodeValue();
				}
				osmNodes.add(new OSMNode(id, latitude, longitude, version, tags));
			}
		}

		if (osmNodes.isEmpty()) {
			throw new IllegalArgumentException("Research provided no results");
		}
		return osmNodes;
	}

	//build the response
	private GetPoiListResponse getPoiInfo(List<OSMNode> l) throws Exception {
		
		List<PoiType> results = new ArrayList<PoiType>();

		for (OSMNode osmNode : l) {
			if (osmNode.getTags().get("name") != null) {
				// create node
				PoiType t = new PoiType();
				//set lat,lon,name
				t.setLat(Double.parseDouble(osmNode.getLat()));
				t.setLon(Double.parseDouble(osmNode.getLon()));
				t.setName(osmNode.getTags().get("name"));
				
				//set type
				String type;
				if (osmNode.getTags().get("highway") != null) {
					type = osmNode.getTags().get("highway");
					t.setType(type);
				}
				if (osmNode.getTags().get("amenity") != null) {
					type = osmNode.getTags().get("amenity");
					t.setType(type);

				}
				if (osmNode.getTags().get("tourism") != null) {
					type = osmNode.getTags().get("tourism");
					t.setType(type);
				}
				if (osmNode.getTags().get("historic") != null) {
					type = osmNode.getTags().get("historic");
					t.setType(type);
				}
				//create poiInfo, null if no info available
				PoiInfo pi = null;
				boolean control = false;
				
				//set religion
				ReligionType rl = new ReligionType();

				if (osmNode.getTags().get("religion") != null) {
					control = true;
					String religion = osmNode.getTags().get("religion");
					rl.setReligion(religion);
				}
				if (osmNode.getTags().get("denomination") != null) {
					control = true;
					String denomination = osmNode.getTags().get("denomination");
					rl.setDenomination(denomination);
				}
				if (control) {
					pi = new PoiInfo();
					pi.setReligion(rl);
					control = false;
				}
				
				//set address
				AddressType at = new AddressType();

				if (osmNode.getTags().get("addr:housenumber") != null) {
					control = true;
					String houseNumber = osmNode.getTags().get("addr:housenumber");
					at.setHouseNumber(houseNumber);
				}
				if (osmNode.getTags().get("addr:street") != null) {
					control = true;
					String street = osmNode.getTags().get("addr:street");
					at.setStreet(street);
				}
				if (osmNode.getTags().get("phone") != null) {
					control = true;
					String phone = osmNode.getTags().get("phone");
					at.setPhone(phone);
				}
				if (control) {
					if (pi == null) {
						pi = new PoiInfo();
					}
					pi.setAddress(at);
					control = false;
				}
				
				//set busInfo
				BusInfoType bus = new BusInfoType();

				if (osmNode.getTags().get("route_ref") != null) {
					control = true;
					String lines = osmNode.getTags().get("route_ref");
					bus.setLines(lines);
				}
				if ((osmNode.getTags().get("operator") != null) && (osmNode.getTags().get("highway") != null)) {
					control = true;
					String operator = osmNode.getTags().get("operator");
					bus.setOperator(operator);
				}
				if (osmNode.getTags().get("ref") != null) {
					control = true;
					String id = osmNode.getTags().get("ref");
					bus.setStopId(id);
				}
				if (control) {
					if (pi == null) {
						pi = new PoiInfo();
					}
					pi.setBusInfo(bus);
				}
				if (pi != null) {
					t.setPoiInfo(pi);
				}

				results.add(t);
			}
		}

		GetPoiListResponse response = new GetPoiListResponse();
		response.getPoi().addAll(results);
		return response;
	}
}

package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class ReligionType 
{
    private String religion;
    private String denomination;

    public String getReligion() {
        return religion;
    }

    public void setReligion(String value) {
        this.religion = value;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String value) {
        this.denomination = value;
    }
}

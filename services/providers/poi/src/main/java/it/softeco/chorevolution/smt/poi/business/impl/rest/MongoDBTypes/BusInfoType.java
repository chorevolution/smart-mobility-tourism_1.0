package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class BusInfoType 
{
    private String lines;
    private String operator;
    private String stopId;

    public String getLines() {
        return lines;
    }

    public void setLines(String value) {
        this.lines = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String value) {
        this.operator = value;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String value) {
        this.stopId = value;
    }
}

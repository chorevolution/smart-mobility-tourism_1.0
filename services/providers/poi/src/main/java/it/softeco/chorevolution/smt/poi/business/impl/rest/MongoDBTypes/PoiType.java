package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

public class PoiType 
{
	private double lat;
	private double lon;
	private String type;
	private String name;
	private PoiInfo poiInfo;

	
	
	public double getLat() {
		return lat;
	}

	public void setLat(double value) {
		this.lat = value;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double value) {
		this.lon = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		this.type = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public PoiInfo getPoiInfo() {
		return poiInfo;
	}

	public void setPoiInfo(PoiInfo value) {
		this.poiInfo = value;
	}
}

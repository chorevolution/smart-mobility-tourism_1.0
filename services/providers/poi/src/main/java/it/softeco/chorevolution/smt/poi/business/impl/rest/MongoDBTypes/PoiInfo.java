package it.softeco.chorevolution.smt.poi.business.impl.rest.MongoDBTypes;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class PoiInfo 
{
    private AddressType address;
    private BusInfoType busInfo;
    private ReligionType religion;

    public AddressType getAddress() {
        return address;
    }

    public void setAddress(AddressType value) {
        this.address = value;
    }

    public BusInfoType getBusInfo() {
        return busInfo;
    }

    public void setBusInfo(BusInfoType value) {
        this.busInfo = value;
    }

    public ReligionType getReligion() {
        return religion;
    }

    public void setReligion(ReligionType value) {
        this.religion = value;
    }
}

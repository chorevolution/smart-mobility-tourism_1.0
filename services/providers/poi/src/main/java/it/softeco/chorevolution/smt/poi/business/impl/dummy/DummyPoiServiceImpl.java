package it.softeco.chorevolution.smt.poi.business.impl.dummy;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.poi.GetPoiListResponse;
import it.softeco.chorevolution.smt.poi.Tags;
import it.softeco.chorevolution.smt.poi.business.BusinessException;
import it.softeco.chorevolution.smt.poi.business.PoiService;

@Service
public class DummyPoiServiceImpl implements PoiService {
	
	@Override
	public GetPoiListResponse getPoiList(Double lat, Double lon, Integer vicinityRange, List<Tags> tags, String session) throws BusinessException {
		GetPoiListResponse response = new GetPoiListResponse();
		return response;
	}

	
}

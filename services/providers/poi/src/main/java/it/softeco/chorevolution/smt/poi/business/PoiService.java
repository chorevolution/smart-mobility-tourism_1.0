package it.softeco.chorevolution.smt.poi.business;

import java.util.List;

import it.softeco.chorevolution.smt.poi.GetPoiListResponse;
import it.softeco.chorevolution.smt.poi.Tags;

public interface PoiService {
	
	GetPoiListResponse getPoiList(Double lat, Double lon, Integer vicinityRange, List<Tags> tags, String session) throws BusinessException;
}

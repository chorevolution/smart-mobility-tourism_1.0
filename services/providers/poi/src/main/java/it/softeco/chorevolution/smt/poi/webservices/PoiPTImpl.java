package it.softeco.chorevolution.smt.poi.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.poi.GetPoiListRequest;
import it.softeco.chorevolution.smt.poi.GetPoiListResponse;
import it.softeco.chorevolution.smt.poi.PoiPT;
import it.softeco.chorevolution.smt.poi.business.PoiService;

@Component(value = "PoiPTImpl")
public class PoiPTImpl implements PoiPT {

	private static Logger logger = LoggerFactory.getLogger(PoiPTImpl.class);
	
	@Autowired
	private PoiService service;

	@Override
	public GetPoiListResponse getPoiList(GetPoiListRequest parameters) {
		
		try {
			logger.info("CALLED getPoiList ON poi");

			GetPoiListResponse response = service.getPoiList(parameters.getLat(), parameters.getLon(), parameters.getRange(),
					parameters.getTag(), parameters.getSession());
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}
}

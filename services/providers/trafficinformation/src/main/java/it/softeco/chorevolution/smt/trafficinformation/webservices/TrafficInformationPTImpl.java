package it.softeco.chorevolution.smt.trafficinformation.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.trafficinformation.TrafficInformationPT;
import it.softeco.chorevolution.smt.trafficinformation.TrafficRequest;
import it.softeco.chorevolution.smt.trafficinformation.TrafficResponse;
import it.softeco.chorevolution.smt.trafficinformation.business.TrafficInformationService;

@Component(value = "TrafficInformationPTImpl")
public class TrafficInformationPTImpl implements TrafficInformationPT {

	private static Logger logger = LoggerFactory.getLogger(TrafficInformationPTImpl.class);
	
	@Autowired
	private TrafficInformationService service;

	public TrafficResponse traffic(TrafficRequest parameters) {

		logger.info("CALLED traffic ON Traffic Information");

		try {
			TrafficResponse trafficResponse = service.getTrafficInfo(parameters);
			return trafficResponse;
		} 
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}
}

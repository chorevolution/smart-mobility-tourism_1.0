package it.softeco.chorevolution.smt.trafficinformation.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.trafficinformation.TrafficMessageType;
import it.softeco.chorevolution.smt.trafficinformation.TrafficRequest;
import it.softeco.chorevolution.smt.trafficinformation.TrafficResponse;
import it.softeco.chorevolution.smt.trafficinformation.business.BusinessException;
import it.softeco.chorevolution.smt.trafficinformation.business.TrafficInformationService;

@Service
public class DummyTrafficInformationServiceImpl implements TrafficInformationService {

	@Override
	public TrafficResponse getTrafficInfo(TrafficRequest parameters) throws BusinessException {
		TrafficResponse response = new TrafficResponse();
		TrafficMessageType element = new TrafficMessageType();
		element.setDesc("test");
		response.getTrafficMessage().add(element);
		return response;
	}
	
	

}

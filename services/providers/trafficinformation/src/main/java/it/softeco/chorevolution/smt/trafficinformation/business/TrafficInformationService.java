package it.softeco.chorevolution.smt.trafficinformation.business;

import it.softeco.chorevolution.smt.trafficinformation.TrafficRequest;
import it.softeco.chorevolution.smt.trafficinformation.TrafficResponse;

public interface TrafficInformationService {

	TrafficResponse  getTrafficInfo(TrafficRequest parameters) throws BusinessException;
}

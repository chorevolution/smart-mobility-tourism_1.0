package it.softeco.chorevolution.smt.news.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.news.GetInfoNewsResponse;
import it.softeco.chorevolution.smt.news.business.BusinessException;
import it.softeco.chorevolution.smt.news.business.NewsService;

@Service
public class DummyNewsServiceImpl implements NewsService {
	
	@Override
	public GetInfoNewsResponse getNews() throws BusinessException {
		GetInfoNewsResponse response = new GetInfoNewsResponse();
		return response;
	}
	
	

}

package it.softeco.chorevolution.smt.news.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.news.GetInfoNewsResponse;
import it.softeco.chorevolution.smt.news.NewsInfoItemType;
import it.softeco.chorevolution.smt.news.business.BusinessException;
import it.softeco.chorevolution.smt.news.business.NewsService;

@Service
public class RESTNewsServiceImpl implements NewsService {

	private static Logger logger = LoggerFactory.getLogger(RESTNewsServiceImpl.class);

	@Value("#{cfgproperties.mesurl}")
	private String mesurl;

	@Value("#{cfgproperties.methodget}")
	private String methodget;

	@Override
	public GetInfoNewsResponse getNews() throws BusinessException {

		try {
			String baseUrl = mesurl + methodget + "?collection=VisitGenoa";

			// create the url
			String url = baseUrl + "&period=" + "3600";

			// connect to url
			HttpURLConnection c = null;

			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				GetInfoNewsResponse resp = new GetInfoNewsResponse();

				// pick docs array from json
				JSONObject obj = new JSONObject(sb.toString());
				JSONArray docs = obj.getJSONArray("docs");
				// iterate all news and pick the first 20
				for (int i = 0; i < docs.length(); i++) 
				{
					//event
					JSONObject event = docs.getJSONObject(i).getJSONObject("event");
					NewsInfoItemType temp = new NewsInfoItemType();
					temp.setTitle(event.getString("name"));
					temp.setLocation(event.getString("location_name"));
					temp.setIssueDate(event.getString("date_start"));
					temp.setValidityDate(event.getString("date_end"));
					
					//check if the event is still valid (if today date it's contained inside the start-end range)
					//if valid, add it to the response
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date startDate = format.parse(temp.getIssueDate());
					Date endDate = format.parse(temp.getValidityDate());
					Date currentDate = new Date();
					
					if(!currentDate.before(startDate) && !currentDate.after(endDate) && resp.getInfoItem().size() < 15)
						resp.getInfoItem().add(temp);
				}

				return resp;

			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}



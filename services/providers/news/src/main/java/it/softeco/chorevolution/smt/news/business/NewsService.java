package it.softeco.chorevolution.smt.news.business;

import it.softeco.chorevolution.smt.news.GetInfoNewsResponse;

public interface NewsService {

	GetInfoNewsResponse getNews() throws BusinessException;
}

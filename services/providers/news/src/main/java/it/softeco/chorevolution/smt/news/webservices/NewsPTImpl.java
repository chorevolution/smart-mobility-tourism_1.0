package it.softeco.chorevolution.smt.news.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.news.GetInfoNewsRequest;
import it.softeco.chorevolution.smt.news.GetInfoNewsResponse;
import it.softeco.chorevolution.smt.news.NewsPT;
import it.softeco.chorevolution.smt.news.business.NewsService;

@Component(value = "NewsPTImpl")
public class NewsPTImpl implements NewsPT {

	private static Logger logger = LoggerFactory.getLogger(NewsPTImpl.class);
	
	@Autowired
	private NewsService service;

	@Override
	public GetInfoNewsResponse getInfo(GetInfoNewsRequest parameters) {

		logger.info("CALLED getInfo ON NEWS");

		try {
			GetInfoNewsResponse response = service.getNews();
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}
}

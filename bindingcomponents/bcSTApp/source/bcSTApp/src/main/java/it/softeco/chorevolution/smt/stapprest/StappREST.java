package it.softeco.chorevolution.smt.stapprest;
 
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
 
 
@Controller
public class StappREST{
	
	private static Logger logger = LoggerFactory.getLogger(StappREST.class);
	
    @GET
    @Produces("text/plain")
    @Path("/getTripPlan")
    public String getTripPlan(@QueryParam("lat")double lat, @QueryParam("lon")double lon, @QueryParam("range")int range, @QueryParam("tag")List<String> tags,
                @QueryParam("tripBy")int tripBy) {
                
				logger.info("CALLED getTripPlan ON bcSTApp");
    	
                try
                {
                		//local tomcat
                		//URL url = new File("./webapps/stapprest/WEB-INF/classes/cdstapp.wsdl").toURI().toURL();
                		//eclipse tomcat
                		//URL url = StappREST.class.getResource("/cdstapp.wsdl");
                		
                		STAppService cdstAppService = new STAppService();
                		STAppPT cdstAppPT = cdstAppService.getSTAppPort();
                		
                		GetTripPlanRequest getTripPlanRequest = new GetTripPlanRequest();
                		getTripPlanRequest.setLat(lat);
                		getTripPlanRequest.setLon(lon);
                		getTripPlanRequest.setRange(range);
                		//build poitypes from the list passed
                		List<Tags> poiTypes = new ArrayList<Tags>();
                		for (String poyType : tags)
                		{
							Tags t = Tags.fromValue(poyType);
							poiTypes.add(t);
						}
                		getTripPlanRequest.getTag().addAll(poiTypes);
                		getTripPlanRequest.setTripBy(tripBy);
                		GetTripPlanResponse getTripPlanResponse = cdstAppPT.getTripPlan(getTripPlanRequest);

                        //convert the planResponse to json
                        ObjectMapper mapper = new ObjectMapper();
                        String json = mapper.writeValueAsString(getTripPlanResponse);
                        return json;
                }
                catch (Exception e)
                {
                        throw new RuntimeException(e.getMessage());
                } 
    }
}
package it.softeco.chorevolution.smt.weather.business;

import it.softeco.chorevolution.smt.weather.WeatherInfoByAreaResponse;
import it.softeco.chorevolution.smt.weather.WeatherInformationResponse;

public interface WeatherService {

	WeatherInfoByAreaResponse getMeteoInfoByArea(Integer period, Double lat, Double lon, String tag, Double radius) throws BusinessException;

	WeatherInformationResponse getMeteoInfo(Integer period) throws BusinessException;

}

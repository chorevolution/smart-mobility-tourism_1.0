package it.softeco.chorevolution.smt.weather.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.weather.WeatherInfoByAreaResponse;
import it.softeco.chorevolution.smt.weather.WeatherInformationResponse;
import it.softeco.chorevolution.smt.weather.business.BusinessException;
import it.softeco.chorevolution.smt.weather.business.WeatherService;

@Service
public class DummyWeatherServiceImpl implements WeatherService {

	@Override
	public WeatherInfoByAreaResponse getMeteoInfoByArea(Integer period, Double lat, Double lon, String tag, Double radius) throws BusinessException {
		WeatherInfoByAreaResponse response = new WeatherInfoByAreaResponse();
		response.setStatus("test-status");
		return response;
	}

	@Override
	public WeatherInformationResponse getMeteoInfo(Integer period) throws BusinessException {
		WeatherInformationResponse response = new WeatherInformationResponse();
		response.setStatus("test-status");
		return response;
	}
	
	

}

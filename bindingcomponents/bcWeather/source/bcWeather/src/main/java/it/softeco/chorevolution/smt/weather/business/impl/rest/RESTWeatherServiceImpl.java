package it.softeco.chorevolution.smt.weather.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.softeco.chorevolution.smt.weather.WeatherInfoByAreaResponse;
import it.softeco.chorevolution.smt.weather.WeatherInformationResponse;
import it.softeco.chorevolution.smt.weather.business.BusinessException;
import it.softeco.chorevolution.smt.weather.business.WeatherService;

@Service
public class RESTWeatherServiceImpl implements WeatherService {

	private static Logger logger = LoggerFactory.getLogger(RESTWeatherServiceImpl.class);

	@Value("#{cfgproperties.mesurl}")
	private String mesurl;

	@Value("#{cfgproperties.methodget}")
	private String methodget;
	
	@Value("#{cfgproperties.methodgetinarea}")
	private String methodgetinarea;

	@Override
	public WeatherInfoByAreaResponse getMeteoInfoByArea(Integer period, Double lat, Double lon, String tag, Double radius) throws BusinessException {

		try {
			String baseUrl = mesurl + methodgetinarea + "?collection=weather";

			// create the url
			String url = baseUrl + "&period=" + period + "&lat=" + lat + "&lon=" + lon + (tag != null ? "&tag=" + tag : "") + "&radius=" + radius;

			// connect to url
			HttpURLConnection c = null;

			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				// map the json response to java classes
				Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
				WeatherInfoByAreaResponse resp = gson.fromJson(sb.toString(), WeatherInfoByAreaResponse.class);
				return resp;

			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}

	@Override
	public WeatherInformationResponse getMeteoInfo(Integer period) throws BusinessException {

		try {
			String baseUrl = mesurl + methodget + "?collection=weather";

			// create the url
			String url = baseUrl + "&period=" + period;

			// connect to url
			HttpURLConnection c = null;

			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				// map the json response to java classes
				Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
				WeatherInformationResponse resp = gson.fromJson(sb.toString(), WeatherInformationResponse.class);
				return resp;

			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}

}

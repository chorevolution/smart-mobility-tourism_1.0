package it.softeco.chorevolution.smt.weather.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.weather.WeatherInfoByAreaRequest;
import it.softeco.chorevolution.smt.weather.WeatherInfoByAreaResponse;
import it.softeco.chorevolution.smt.weather.WeatherInformationRequest;
import it.softeco.chorevolution.smt.weather.WeatherInformationResponse;
import it.softeco.chorevolution.smt.weather.WeatherPT;
import it.softeco.chorevolution.smt.weather.business.WeatherService;

@Component(value = "WeatherPTImpl")
public class WeatherPTImpl implements WeatherPT {

	private static Logger logger = LoggerFactory.getLogger(WeatherPTImpl.class);

	@Autowired
	private WeatherService service;

	public WeatherInfoByAreaResponse weatherInfoByArea(WeatherInfoByAreaRequest parameters) {

		logger.info("CALLED weatherInfoByArea ON bcWeather");

		try {
			WeatherInfoByAreaResponse resp = service.getMeteoInfoByArea(parameters.getPeriod(), parameters.getLat(), parameters.getLon(), parameters.getTag(), parameters.getRadius());

			return resp;
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}

	}

	@Override
	public WeatherInformationResponse weatherInformation(WeatherInformationRequest parameters) {

		logger.info("CALLED weatherInformation ON bcWeather");
		try {
			WeatherInformationResponse resp = service.getMeteoInfo(parameters.getPeriod());
			return resp;
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}

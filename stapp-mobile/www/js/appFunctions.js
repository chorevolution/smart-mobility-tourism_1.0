angular.module('appFunctions', ['ionic'])

function checkParams($scope, $ionicPopup)
{
    if (angular.isUndefined($scope.range) ||
        $scope.poiType.length==0 || 
        angular.isUndefined($scope.tripBy)) 
    {
        $ionicPopup.alert
        ({
            content: 'Please fill all parameters',
            okType: 'button-dark'
        })
        resetInput($scope);
        return false;
    }
    return true;
}

function resetInput($scope)
{
    $scope.range = undefined;
    $scope.tripBy = undefined;
    $scope.poiType = undefined;
}
angular.module('callTouristAgent', [])

function addCurrentPosition($scope)
{
    $scope.markers.push({
            lat: $scope.current.lat,
            lng: $scope.current.lng,
                message: "Current position",
                icon: {
                    type: 'makiMarker',
                    icon: 'marker',
                    color: '#397D02',
                    size: "l"
                }
    })
}

function addPois($scope, taResponse)
{
    var response = taResponse.get();
    if(response.poi!=null && response.poi.length>0)
    {
        //iterate pois
        for (var i = 0; i < response.poi.length; i++)
        {
            //pick the icon relative to poiType
            var iconType="";
            switch(response.poi[i].type)
            {
                case "restaurant":
                    iconType="restaurant";
                    break;
                case "hospital":
                    iconType="hospital";
                    break;
                case "school":
                    iconType="school";
                    break; 
                case "place_of_worship":
                    iconType="place-of-worship";
                    break;
                default :
                    iconType="town-hall";
                    break;
            }
            //add the marker on map
            if(iconType!="town-hall")
            {
                $scope.markers.push
                ({
                    lat: response.poi[i].lat,
                    lng: response.poi[i].lon,
                    message: response.poi[i].name,
                    icon: 
                    {
                        type: 'makiMarker',
                        icon: iconType,
                        color: '#f00'
                    }
                })
            }
            else
            {
                $scope.markers.push
                ({
                    lat: response.poi[i].lat,
                    lng: response.poi[i].lon,
                    message: response.poi[i].name + ' (' + response.poi[i].type + ')',
                    icon: 
                    {
                        type: 'makiMarker',
                        icon: iconType,
                        color: '#f00'
                    }
                })
            }
        }
    }
}

function addParkings($scope, taResponse)
{
    var response = taResponse.get();
     if(response.parkings.length > 0)
    {
        for (var i = 0; i < response.parkings.length; i++)
        {
                $scope.markers.push({
                lat: response.parkings[i].lat,
                lng: response.parkings[i].lon,
                    icon: {
                    type: 'makiMarker',
                    icon: 'parking',
                    color: '#00008B'
                    }
                })
        }
    }
}

function addTrafficInfo($scope, taResponse)
{
    var response = taResponse.get();
    if(response.trafficMessage.length > 0)
    {
        for (var i = 0; i < response.trafficMessage.length; i++)
        {
                $scope.markers.push({
                lat: response.trafficMessage[i].x,
                lng: response.trafficMessage[i].y,
                message: response.trafficMessage[i].desc,
                    icon: {
                    type: 'makiMarker',
                    icon: 'roadblock',
                    color: '#00008B'
                    }
                })
        }
    }
}

function addTripNodes($scope, segments, response, tripsCounter)
{
    //startNode
    var startSegment = segments[0];
    $scope.markers.push({
        lat: startSegment.positions[0].lat,
        lng: startSegment.positions[0].lon,
        message: response.trips[tripsCounter].fromPoiName,
            icon: {
            type: 'makiMarker',
            icon: 'marker',
            color: '#f00'
            }
    })
    //endNode
    var endSegment = segments[segments.length-1];
    var endSegmentPositions = endSegment.positions;
    $scope.markers.push({
            lat: endSegmentPositions[endSegmentPositions.length-1].lat,
            lng: endSegmentPositions[endSegmentPositions.length-1].lon,
            message: response.trips[tripsCounter].toPoiName,
                icon: {
                type: 'makiMarker',
                icon: 'embassy',
                color: '#f00'
                }
    })
}

function addBusInfo($scope, segments, i)
{
    var positions = segments[i].positions;

    //dropin node
    $scope.markers.push({
        lat: positions[0].lat,
        lng: positions[0].lon,
        message: "Line: " + segments[i].busDetails.line + " Direction: " +
        segments[i].busDetails.direction,
        icon: {
            type: 'makiMarker',
            icon: 'bus',
            markerColor: '#f00'
            }
    })
    //dropoff node
    $scope.markers.push({
        lat: positions[positions.length-1].lat,
        lng: positions[positions.length-1].lon,
        message: "Bus Dropoff",
        icon: {
            type: 'makiMarker',
            icon: 'bus',
            markerColor: '#f00'
            }
    })
}

function showWeather($scope, response)
{
    if(response.weather[0] != null)
    {
        document.getElementById('weatherInfo').style.visibility = 'visible';
        if(response.weather[0].temperature != null)
            $scope.temperature = response.weather[0].temperature + " °C";
        if(response.weather[0].pressure != null)
            $scope.pressure = response.weather[0].pressure + " mb";
        if(response.weather[0].humidity != null)
            $scope.humidity = response.weather[0].humidity + " %";
        if(response.weather[0].rain != null)
            $scope.rain = response.weather[0].rain + " mm";
    }
}

function showDetails($scope, tripSegments)
{
    //get the segments and show them
    var segments = tripSegments.get();
    if(segments.length > 0)
    {
        $scope.tripDetails = [];
        for(var i=0; i<segments.length; i++)
        {
            $scope.tripDetails.push(segments[i]);
        }
        document.getElementById('tripInfo').style.visibility = 'visible';
    }    
}

function pushDetails($scope, segments, i, tripSegments, fromName, toName)
{
    var transpType = segments[i].type;
    tripSegments.add(fromName, toName, transpType);
}

function showPublicTransportInfo($scope, response)
{
    if(response.publicTransportationInfos[0] != null)
    {
        document.getElementById('publicTransportInfo').style.visibility = 'visible';
        //save title and text of all infos
        $scope.publicTranspInfos = [];
        $scope.publicTranspInfosDetailed = [];    
        for(var i=0; i<response.publicTransportationInfos.length; i++)
        {
            $scope.publicTranspInfos.push(response.publicTransportationInfos[i].issueDate+" - "+response.publicTransportationInfos[i].title);
            $scope.publicTranspInfosDetailed.push(response.publicTransportationInfos[i].text);
        }
    }
}

function showNews($scope, response)
{
    if(response.news[0] != null)
    {
        var newsDiv = document.getElementById('news');
        //show news on top of tab if others are null
        if(response.publicTransportationInfos.length==0 && response.weather.length==0)
        {
            newsDiv.style.top = "10%";
            newsDiv.style.bottom = "50%";
        }
        newsDiv.style.visibility = 'visible';
        //save text of all news
        $scope.news = [];
        for(var i=0; i<response.news.length; i++)
        {
            $scope.news.push(response.news[i]);
        }
    }
}

function addSegments($scope, segments, response, tripsCounter, tripSegments)
{
    for(var i=0; i<segments.length; i++)
    {
        //name of the start point of the segment
        var fromName="";
        if(i==0)
            fromName=response.trips[tripsCounter].fromPoiName;
        else
        {
            //if it's a bus segment, pick the dropIn
            if(segments[i].type=="Bus")
                fromName="DropIn Line "+segments[i].busDetails.line;
            //if it's a walk segment, it starts from a bus dropOff
            if(segments[i].type=="Walk")
                fromName="Bus DropOff";
        }
        //name of the end point of the segment
        var toName="";
        if(i==segments.length-1)
            toName=response.trips[tripsCounter].toPoiName;
        else
        {
            //the end point it's a bus stop
            if(segments[i].type=="Walk" || (segments[i].type=="Bus" && segments[i+1].type=="Bus"))
                toName="DropIn Line "+segments[i+1].busDetails.line;
            //the end point it's a bus DropOff
            else
                toName="Bus DropOff";
        }
        //put the segment details in memory
        pushDetails($scope, segments, i, tripSegments, fromName, toName);
        //if it's a bus segment add infos on the map
        if(segments[i].type == "Bus")
        {
            addBusInfo($scope, segments, i);
        }
        //add coordinates of the trip
        var latlngs = [];
        for(j=0; j<segments[i].positions.length; j++)
        {
            latlngs.push({
                        "lat": segments[i].positions[j].lat,
                        "lng": segments[i].positions[j].lon
                    })

            $scope.trip.push({
                    "color": "green",
                    "weight": 3,
                    "latlngs": latlngs
            })
        }
    }
}

function cleanMap($scope, response, tripsCounter)
{
    //clean map from prev trips info
    angular.extend($scope, {
            current: {
                zoom: 14,
                lat: 44.4018166,
                lng: 8.942768
            },
            markers : $scope.markers,
            paths: $scope.trip
    })
    //error message
    $ionicPopup.alert({
        content: 'No trip found between ' + response.trips[tripsCounter].fromPoiName + ' and ' 
        + response.trips[tripsCounter].toPoiName,
        okType: 'button-dark'
    })
}

function showPois($scope, $ionicPopup, $http, taResponse, $rootScope) 
{
    //block control of page
    document.getElementById('disablingDiv').style.display='block';
    //start loading animation
    document.getElementById('loading').style.visibility = 'visible';

    //build the tag rest parameter
    var tags="";
    for(i=0; i<$scope.poiType.length; i++)
        tags+='&tag='+$scope.poiType[i].name;

    $http.get('http://localhost:9090/bcSTApp/services/getTripPlan?lat='+$scope.current.lat+'&lon='+$scope.current.lng+'&range='+$scope.range+tags+'&tripBy='+$scope.tripByConverted+'').
    //$http.get('http://192.168.150.144:8000/bcSTApp/services/getTripPlan?lat='+$scope.current.lat+'&lon='+$scope.current.lng+'&range='+$scope.range+tags+'&tripBy='+$scope.tripByConverted+'').                           
    //$http.get('http://ec2-34-250-43-91.eu-west-1.compute.amazonaws.com:8000/bcSTApp/services/getTripPlan?lat='+$scope.current.lat+'&lon='+$scope.current.lng+'&range='+$scope.range+tags+'&tripBy='+$scope.tripByConverted+'').  
        success(function(data)
        {
            //save response
            taResponse.add(data);
            //markers
            $scope.markers = [];  
            //add current position to markers
            addCurrentPosition($scope);
            //add markers for retrieved pois
            addPois($scope, taResponse);
            //add parking markers if available (trips by car option)
            addParkings($scope, taResponse);
            //add traffic info markers if available (trips by car option)
            addTrafficInfo($scope, taResponse);
            //load markers to the map
            angular.extend($rootScope, {
                current: {
                    zoom: 14,
                    lat: 44.4018166,
                    lng: 8.942768
                    //autoDiscover: true
            },
            markers : $scope.markers
            })
            //make trips button visible
            document.getElementById('nextTrip').style.visibility = 'visible';
            //end loading animation
            document.getElementById('loading').style.visibility = 'hidden';
            //enable control of page
            document.getElementById('disablingDiv').style.display='none';
        })
        .error(function(data)
        {
            //end loading animation
            document.getElementById('loading').style.visibility = 'hidden';
            //enable control of page
            document.getElementById('disablingDiv').style.display='none';
            //error message
            $ionicPopup.alert({
                content: 'FAILED TO CONTACT SERVICE',
                okType: 'button-dark'
            })
        })
}

function showPaths($scope, $ionicPopup, tripSegments, taResponse)
{
    var response = taResponse.get();
    var tripsCounter = taResponse.getCounter();
    if(tripsCounter == response.trips.length)
    {
        $ionicPopup.alert({
            content: 'TRIPS COMPLETED',
            okType: 'button-dark'
        })
        return;
    }
    if(tripsCounter >= 0)
    {
        document.getElementById('nextTrip').style.display='none';
        //show the next trip slide button
        document.getElementById('next').style.visibility = 'visible';
    }
    //reset objects
    $scope.trip = []; 
    $scope.markers = [];
    tripSegments.clear();

    //pick duration time of the current trip
    $scope.duration = Math.floor((response.trips[tripsCounter].duration)/60);
    
    var segments = response.trips[tripsCounter].segments;
    //if trips informations not empty 
    if(segments.length > 0)
    {
        //add startNode and endNode
        addTripNodes($scope, segments, response, tripsCounter);
        //add segments
        addSegments($scope, segments, response, tripsCounter, tripSegments);
        //show path and center the map on start point
        angular.extend($scope, {
            current: {
                zoom: 14,
                lat: segments[0].positions[0].lat,
                lng: segments[0].positions[0].lon
            },
            markers : $scope.markers,
            paths: $scope.trip
        })
    }
    else
    {
        //clean map from prev trips info
        cleanMap($scope, response, tripsCounter)
    }
    //increment tripCounter
    taResponse.sum();
    //show the tripInformations
    showDetails($scope, tripSegments);
}

function showInfo($scope, taResponse)
{
    var response = taResponse.get();
    if(response != null)
    {
        showWeather($scope, response);
        showPublicTransportInfo($scope, response);
        showNews($scope, response);
    }
} 





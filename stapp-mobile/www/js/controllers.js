angular.module('controllers', 
['leaflet-directive',  
 'ionic-modal-select',
 'angular.filter'])

//navigation bar controller
.controller("BarCTRL", function($scope, $ionicPopup, $http, taResponse, $rootScope) {
        // refresh page button
        $scope.refresh = function()
        {
             window.location.reload(true);
        };
        //choose poiTypes popup
        $scope.poiTypesChange = function()
        {
            //declare a scope variable for the checkbox
            $scope.tags = {};
            
            //open a popup with a checkbox
            $ionicPopup.show
            ({
                title: 'Select types of poi',
                content: '<ion-checkbox class="my-checkbox" ng-model="tags.hospital">'+
                            'hospital'+
                        '</ion-checkbox>'+
                        '<ion-checkbox class="my-checkbox" ng-model="tags.restaurant">'+
                            'restaurant'+
                        '</ion-checkbox>'+
                        '<ion-checkbox class="my-checkbox" ng-model="tags.school">'+
                            'school'+
                        '</ion-checkbox>'+
                        '<ion-checkbox class="my-checkbox" ng-model="tags.tourism">'+
                            'tourism'+
                        '</ion-checkbox>'+
                        '<ion-checkbox class="my-checkbox" ng-model="tags.worship">'+
                            'worship'+
                        '</ion-checkbox>',
                scope: $scope,
                buttons: 
                [
                    { text: 'Cancel'},
                    { text: 'OK', type: 'button-dark', onTap: function(e) { return e;}}
                ]
            })
            .then(function(res) 
            {
                if (res)
                {
                    //build the poiType parameter looking for selected poiTypes in checkbox
                    $scope.poiType=[];

                    if($scope.tags.hospital==true)
                        $scope.poiType.push({name : "hospital"});    
                    if($scope.tags.restaurant==true)
                        $scope.poiType.push({name : "restaurant"});    
                    if($scope.tags.school==true)
                        $scope.poiType.push({name : "school"});    
                    if($scope.tags.tourism==true)
                        $scope.poiType.push({name : "tourism"});    
                    if($scope.tags.worship==true)
                        $scope.poiType.push({name : "worship"});    
                    
                    //call the next popup
                    $scope.tripByChange();
                }
                else
                {
                    return;
                }
            });
        }
        //choose type of transport
        $scope.tripByChange = function()
        {  
            $scope.transports =
            [
                { title: "Bus", checked: false },
                { title: "Car", checked: false },
                { title: "Walk", checked: false },
            ];

            $scope.updateSelection = function(position, transports, title) 
            {
                angular.forEach(transports, function(subscription, index) 
                {
                    if (position != index)
                        subscription.checked = false;
                        $scope.selected = title;
                });
            }
            
            //open a popup with a checkbox
            $ionicPopup.show
            ({
                title: 'Select types of transport',
                content: '<ion-list>'+
                        '<ion-item class="item-checkbox" ng-repeat="item in transports">'+
                        '<label class="checkbox">'+
                        '<input type="checkbox" ng-model="item.checked" ng-click="updateSelection($index, transports, item.title)">'+
                        '</label>'+
                        '{{ item.title }}'+
                        '</ion-item>'+
                        '</ion-list>',
                scope: $scope,
                buttons: 
                [
                    { text: 'Cancel'},
                    { text: 'OK', type: 'button-dark', onTap: function(e) { return e;}}
                ]
            })
            .then(function(res) 
            {
                if (res)
                {
                    //check which transport was selected
                    for(var i=0; i<$scope.transports.length; i++)
                    {
                        if($scope.transports[i].checked == true)
                        {
                            $scope.tripBy = $scope.transports[i].title;
                            break;
                        }
                    }
                    //call the next popup
                    $scope.openRangePrompt();
                }
                else
                {
                    return;
                }
            });
        }
        //insert range popup
        $scope.openRangePrompt = function()
        {
            $ionicPopup.prompt
            ({
                title: 'Insert range for the search (meters)',
                inputType: 'tel',
                inputPlaceholder: 'Range:'
            })
            .then(function(res)
            {
                if(res)
                {
                    //set value
                    $scope.range = res;
                    //call the REST getTripPlan
                    $scope.callRest();
                }
                else
                {
                    $scope.callRest();
                }
            });
        }
        //call stapp rest
        $scope.callRest = function()
        {
            //check parameters
            var control = checkParams($scope, $ionicPopup);
            if(control == false)
                return;
            //convert tripBy parameter
            $scope.tripByConverted;
            if($scope.tripBy=="Walk")
                $scope.tripByConverted=1;
            else if($scope.tripBy=="Car")
                $scope.tripByConverted=8;
            else if($scope.tripBy=="Bus")
                $scope.tripByConverted=4;

            //build the string containing all tag selected
            var selectedTags="";
            for(i=0; i<$scope.poiType.length; i++)
                selectedTags=selectedTags.concat($scope.poiType[i].name+" ");

            //confirm the search
            $ionicPopup.confirm
            ({
                title: 'Search with these parameters?',
                template: 'PoiTypes: ' + selectedTags + '<br>' +
                'TripBy: ' + $scope.tripBy + '<br>' +
                'Range: ' + $scope.range
            })
            .then(function(res)
            {
                if (res) 
                {
                    //call stapp Rest service and show results on map
                    showPois($scope, $ionicPopup, $http, taResponse, $rootScope);
                }
                else 
                {
                    //reset button values
                    resetInput($scope);
                }
            });
        }

})

// home controller
 .controller("HomeCTRL", function($scope, $ionicPopup, tripSegments, taResponse, $rootScope) {

    //load the empty map in the current position
    angular.extend($rootScope, {
        current: {
            zoom: 14,
            lat: 44.4018166,
            lng: 8.942768
            //autoDiscover: true
        },
        markers: {},
        paths: {}
    })
    
    //show trips on map
    $scope.showPaths = function()
    {
        showPaths($scope, $ionicPopup, tripSegments, taResponse);
    }
})

//info controller
.controller("InfoCTRL", function($scope, $ionicPopup, taResponse) {

   showInfo($scope, taResponse);
   $scope.openDetails = function(index)
   {
       $ionicPopup.alert
        ({
            content: $scope.publicTranspInfosDetailed[index],
            okType: 'button-dark'
        })
   }
})
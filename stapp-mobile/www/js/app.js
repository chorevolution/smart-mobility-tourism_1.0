// Ionic Starter App
angular.module('starter',
  ['factories',
    'controllers',
    'ionic'
  ])

  //starting app function
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Ke0yboard.disableScroll(true);
      }
      if (window.StatusBar)
        StatusBar.styleDefault();
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    //render Ios and Android tabs in the same way
    $ionicConfigProvider.tabs.position('bottom');

    //tab urls mapping
    $stateProvider
      .state('tabs', {
        url: "/tab",
        abstract: true,
        templateUrl: "js/templates/tabs.html"
      })
      .state('tabs.home', {
        url: "/home",
        views: {
          'home-tab': {
            templateUrl: "js/templates/home.html",
            controller: 'HomeCTRL'
          }
        }
      })
      .state('tabs.info', {
        cache: false,
        url: "/info",
        views: {
          'info-tab': {
            templateUrl: "js/templates/info.html",
            controller: 'InfoCTRL'
          }
        }
      })
    $urlRouterProvider.otherwise("/tab/home");
  })





















// Ionic Starter App
angular.module('factories', [])

//factory for share trips informations between controllers
.factory('tripSegments', function()
{
  var segments = [];

  return {
  add: function(fromName, toName, transpType)
  {
  segments.push({from: fromName, to:toName, transportType: transpType});
  },
  get: function()
  {
  return segments;
  },
  clear: function()
  {
  segments=[];
  }
  }
})

//factory for share TouristAgent response and trips counter between controllers
.factory('taResponse', function()
{
  var TAResponse = {};
  var tripCounter = 0;

  return {
  sum: function()
  {
  tripCounter++;
  },
  getCounter: function()
  {
  return tripCounter;
  },
  add: function(response)
  {
  TAResponse=response;
  },
  get: function()
  {
  return TAResponse;
  }
  }
})





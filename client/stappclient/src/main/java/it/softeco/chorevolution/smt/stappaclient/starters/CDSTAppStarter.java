package it.softeco.chorevolution.smt.stappaclient.starters;

import it.softeco.chorevolution.smt.stappclient.client.cdstapp.GetTripPlanRequest;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.GetTripPlanResponse;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.STAppPT;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.STAppService;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.StartTrackingRequest;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.StartTrackingResponse;
import it.softeco.chorevolution.smt.stappclient.client.cdstapp.Tags;

public class CDSTAppStarter {
	
	public static void main(String[] args) {
		
		STAppService cdstAppService = new STAppService();
		
		STAppPT cdstAppPT = cdstAppService.getSTAppPort();
		// call getTripPlan on CD-STApp			
		GetTripPlanRequest getTripPlanRequest = new GetTripPlanRequest();
		getTripPlanRequest.setLat(44.4012166);
		getTripPlanRequest.setLon(8.942768);
		getTripPlanRequest.setRange(250);
		getTripPlanRequest.getTag().add(Tags.RESTAURANT);
		// tripBy = 1 ---> trip Walking
		// tripBy = 4 ---> trip by Public Transportation
		// tripBy = 8 ---> trip By Car
		getTripPlanRequest.setTripBy(1);	
		GetTripPlanResponse getTripPlanResponse = cdstAppPT.getTripPlan(getTripPlanRequest);
		// call startTracking on CD-STApp	
		StartTrackingRequest startTrackingRequest = new StartTrackingRequest();
		StartTrackingResponse startTrackingResponse = cdstAppPT.startTracking(startTrackingRequest);	
	}
}

package it.softeco.chorevolution.smt.stappaclient.starters;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class BCStarter {

	private static Logger logger = LoggerFactory.getLogger(BCStarter.class);
	
	public static void main(String[] args) {
	
		String host = "localhost";
		int port = 9090;		
		double lat = 44.4012166;
		double lon = 8.942768;
		int range = 250;
		String tag = "restaurant";
		// tripBy = 1 ---> trip Walking
		// tripBy = 4 ---> trip by Public Transportation
		// tripBy = 8 ---> trip By Car
		int tripBy = 1;		
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath("/bcSTApp/services/getTripPlan")
					.setParameter("lat",String.valueOf(lat))
					.setParameter("lon",String.valueOf(lon))
					.setParameter("range",String.valueOf(range))
					.setParameter("tag",tag)
					.setParameter("tripBy",String.valueOf(tripBy))
					.build();
			HttpGet getRequest = new HttpGet(uri);		
			httpClient.execute(getRequest);	
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}

	}
}
